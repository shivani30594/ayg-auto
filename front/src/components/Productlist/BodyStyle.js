import React, { Component } from 'react'

class BodyStyle extends Component {
    constructor() {

        super();
        this.state = {            
            selectedOption:""
        };
    }
   componentWillReceiveProps(nextProps) {
      // console.log('nextProps', nextProps);
   }
  render() {
      this.state.selectedOption = this.props.bodyStyle
     
    return (
      <div>
            <div className="filtering-box">
                <h4>Bodystyle</h4>
                <div className="car-main">


                    <div className="car-box">
                        <input type="radio" name="cars" id="cars" value="economical" checked={this.state.selectedOption === 'economical'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars">
                            <i className="car-icon"></i>
                            <span>Economical</span>
                        </label>
                    </div>
                    <div className="car-box">
                        <input type="radio" name="cars" id="cars2" value="sedan" checked={this.state.selectedOption === 'sedan'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars2">
                            <i className="car-icon sedan"></i>
                            <span>Sedan</span>
                        </label>
                    </div>
                    <div className="car-box">
                        <input type="radio" name="cars" id="cars3" value="coupe" checked={this.state.selectedOption === 'coupe'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars3">
                            <i className="car-icon coupe"></i>
                            <span>Coupe</span>
                        </label>
                    </div>

                    <div className="car-box">
                        <input type="radio" name="cars" id="cars4" value="hatchback" checked={this.state.selectedOption === 'hatchback'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars4">
                            <i className="car-icon hatch-back"></i>
                            <span>Hatchback</span>
                        </label>
                    </div>
                    <div className="car-box">
                        <input type="radio" name="cars" id="cars5" value="convertible" checked={this.state.selectedOption === 'convertible'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars5">
                            <i className="car-icon convertible"></i>
                            <span>Convertible</span>
                        </label>
                    </div>
                    <div className="car-box">
                        <input type="radio" name="cars" id="cars6" value="wagon" checked={this.state.selectedOption === 'wagon'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars6">
                            <i className="car-icon wagon"></i>
                            <span>Wagon</span>
                        </label>
                    </div>
                    <div className="car-box">
                        <input type="radio" name="cars" id="cars7" value="suv" checked={this.state.selectedOption === 'suv'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars7">
                            <i className="car-icon suv"></i>
                            <span>SUV</span>
                        </label>
                    </div>
                    <div className="car-box">
                        <input type="radio" name="cars" id="cars8" value="mini-van" checked={this.state.selectedOption === 'mini-van'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars8">
                            <i className="car-icon mini-van"></i>
                            <span>Mini-Van</span>
                        </label>
                    </div>
                    <div className="car-box">
                        <input type="radio" name="cars" id="cars9" value="truck" checked={this.state.selectedOption === 'truck'} onChange={this.props.handleOptionBodyStyle.bind(this)} />
                        <label htmlFor="cars9">
                            <i className="car-icon truck"></i>
                            <span>Truck</span>
                        </label>
                    </div>
                </div>

            </div>
           
      </div>
    )
  }
}
export default BodyStyle;