import React, { Component } from 'react'

const SocialLink = ({ place }) => {
    let classnames = ''
    let extraLi = ''
    if(place === 'header'){
        classnames = "navbar-nav ml-auto social-media";
        extraLi = <li className="call"><a href="#">727.272.4299</a></li>
    }
    if(place === 'footer'){
        classnames = "social-media float-right";
    }
    return (
        <ul className={classnames}>
            {extraLi}
            <li><a href="https://twitter.com/aygauto" className="fa fa-twitter"></a></li>
            <li><a href="https://www.facebook.com/aygauto" className="fa fa-facebook"></a></li>
            <li><a href="https://www.instagram.com/aygauto/" className="fa fa-instagram"></a></li>
            <li><a href="https://goo.gl/maps/JYuvdRKFS8q " className="fa fa-map-marker"></a></li>
        </ul>
    )
}

export default SocialLink;