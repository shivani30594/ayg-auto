import React, { Component } from 'react'
import { Link } from "react-router-dom";
//import { connect } from "react-redux";
//import { bindActionCreators } from "redux";
import { NotificationContainer, NotificationManager } from 'react-notifications';

import carApi from "../../api/carApi";
import FormValue from "../ProductDetail/SelectingFormValuesForm";
import 'react-notifications/lib/notifications.css';
//import Loader from 'react-loaders'

import Modal from "react-responsive-modal";

const styles = {
    fontFamily: "sans-serif",
    textAlign: "center"
};
//const NotificationContainer = window.ReactNotifications.NotificationContainer;
//const NotificationManager = window.ReactNotifications.NotificationManager;
class HeaderLinks extends Component {
constructor(props) {
    super()
    this.state = {
        open: false,
        header:""
    }
}

onCloseModal = () => {
    this.setState({ open: false });
};

contactForm = (h) => {
   
    this.setState({ open: true });
    this.setState({ header: h });
};

onSubmit = (values) => { 
    alert(window.location.href);
    let arr = { "brand": "Not selected", "model_name": "Not selected","link": window.location.href};
    carApi.sentMail(values,arr).then(response=>{
     NotificationManager.success('Your Mail has Successfully Sent ', 'Sent Mail');
    });
    this.setState({ open: false });
}
  
render() {  
    
    return (
        <ul className="navbar-nav mr-auto">
            <li className="nav-item active">
                <Link to="/cars" className="nav-link">
                    Find My Car
                </Link>
                
            </li>
            <li className="nav-item">
                <Link to="/" className="nav-link" value="Get Approved" onClick={this.contactForm.bind(this,"Get Approved")}>
                    Get Approved
                </Link>
                
                <div className="model" style={styles}>
                     <Modal className="schedule-model" open={this.state.open} onClose={this.onCloseModal.bind(this)} >
                        <h2>{this.state.header}</h2>
                        <FormValue onSubmit={this.onSubmit} />
                    </Modal>
                </div>
            </li>
            <li className="nav-item">
                <Link to="/" className="nav-link" onClick={this.contactForm.bind(this, "Get Insured")}>
                    Get Insured
                </Link>
                
            </li>
            <li className="nav-item">
                <Link to="/" className="nav-link" onClick={this.contactForm.bind(this, "Rent A Car")}>
                    Rent A Car
            </Link>

            </li>
            <li className="nav-item">
                <Link to="/" className="nav-link" onClick={this.contactForm.bind(this, "Schedule Concierge")}>
                    Schedule Concierge
            </Link>
            </li>
            <li className="nav-item">

                <a href='https://goo.gl/maps/fd24uqbL8Vv' className="nav-link" target="_blank">
                    About
               </a>
               
            </li>
           
            <NotificationContainer />
        </ul>
    )
  }
}


export default HeaderLinks;