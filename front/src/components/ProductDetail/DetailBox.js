import React, { Component } from 'react';

const img2 = "http://127.0.0.1:8000/cars_images/"

 class DetailBox extends Component {
  render() {
    return <div className="details-box">
        <div className="left">
          <span>2018</span>
          <h4>
            Tesla <span>Model S</span>
          </h4>
          <span>P75</span>
        </div>
        <div className="save">
          <span className="icon">
            <img src={img2} alt=""/>
          </span>
          <h5>SAVE</h5>
        </div>
        <div className="right">
          <span>8k miles</span>
          <h3>$66,900</h3>
        </div>
      </div>;
  }
};
export default DetailBox;