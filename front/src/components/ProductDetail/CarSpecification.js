import React, { Component } from 'react'

const img2 = "http://127.0.0.1:8000/cars_images/"
 class CarSpecification extends Component {
  render() {
    return <div className="product-slider">
        <span className="fullscreen-icon" />
        <span className="fullscreen-close" />
        <div id="productSlider" className="carousel slide">
          <div className="carousel-inner">
            <div className="active item carousel-item" data-slide-number="0">
              <img src={img2} alt=""/>
            </div>
            <div className="item carousel-item" data-slide-number="1">
              <iframe  width="100%" height="100%" src="https://www.youtube.com/embed/Vh5ot31hEGg" frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen />
            </div>
            <a className="carousel-control left pt-3" href="#productSlider" data-slide="prev">
              <i className="fa fa-chevron-left fa-2x" />
            </a>
            <a className="carousel-control right pt-3" href="#productSlider" data-slide="next">
              <i className="fa fa-chevron-right fa-2x" />
            </a>
          </div>

          <ul className="carousel-indicators list-inline">
            <li className="list-inline-item active">
              <a id="carousel-selector-0" className="selected" data-slide-to="0" data-target="#productSlider">
                <img src={img2} className="img-fluid" alt=""/>
              </a>
            </li>
            <li className="list-inline-item">
              <a id="carousel-selector-1" data-slide-to="1" data-target="#productSlider">
                <img src={img2} className="img-fluid" alt="" />
              </a>
            </li>
          </ul>
        </div>
      </div>;
  }
}
export default CarSpecification;