import React, { Component } from 'react';

const img3 = "http://127.0.0.1:8000/cars_images/"

 class DetailSpecification extends Component {
   render() {
     return <div className="specs-details">
         <h3>Details & Specs</h3>
         <div className="specs-details-main">
           <div className="spotlight-box">
             <div className="img">
               <img src={img3} alt="" />
             </div>
             <div className="mpg-details">
               <h5>MPG</h5>
               <div className="city">
                 88
                 <span className="">City</span>
               </div>
               <div className="city">
                 90
                 <span className="text">HWY</span>
               </div>
             </div>
             <div className="transmission">
               <h5>Transmission</h5>
               <span>Auto</span>
             </div>
             <div className="transmission">
               <h5>Drivetrain</h5>
               <span>RWD</span>
             </div>
           </div>

           <div className="basics-main">
             <div className="basics-left">
               <h5>The Basics</h5>
               <table className="table table-striped">
                 <tbody>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                   <tr>
                     <td>Body Style</td>
                     <td>Body Style</td>
                   </tr>
                 </tbody>
               </table>
             </div>
             <div className="advanded-right card-columns">
               <div className="advanded-box card">
                 <h6>Accessory Packages</h6>
                 <ul>
                   <li>
                     <a href="">Supercharger Hardware</a>
                   </li>
                 </ul>
               </div>
               <div className="advanded-box card">
                 <h6>Braking & Traction</h6>
                 <ul>
                   <li>
                     <a href="">ABS (4-wheel)</a>
                   </li>
                   <li>
                     <a href="">Stability Control</a>
                   </li>
                   <li>
                     <a href="">Traction Control</a>
                   </li>
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Comfort & Convenience</h6>
                 <ul>
                   <li>
                     <a href="">The Basics</a>
                   </li>
                   <li>
                     <a href="">Alarm System</a>
                   </li>
                   <li>
                     <a href="">Cruise Control</a>
                   </li>
                   <li>
                     <a href="">Keyless Entry</a>
                   </li>
                   <li>
                     <a href="">Power Windows</a>
                   </li>
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Entertainment & Instrumentation</h6>
                 <ul>
                   <li>
                     <a href="">AM/FM Radio</a>
                   </li>
                   <li>
                     <a href="">Bluetooth Wireless</a>
                   </li>
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Lighting</h6>
                 <ul>
                   <li>
                     <a href="">Daytime Running Lights</a>
                   </li>
                   <li>
                     <a href="">HID Headlamps</a>
                   </li>
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Exterior</h6>
                 <ul>
                   <li>
                     <a href="">Real Spoiler</a>
                   </li>
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Seats</h6>
                 <ul>
                   <li>
                     <a href="">Dual Power Seats</a>
                   </li>
                   <li>
                     <a href="">Heated Seats</a>
                   </li>
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Safety & Security</h6>
                 <ul>
                   <li>
                     <a href="">Backup Camera</a>
                   </li>
                   <li>
                     <a href="">Dual Air Bags</a>
                   </li>
                   <li>
                     <a href="">F&R Parking Sensors</a>
                   </li>
                   <li>
                     <a href="">Head Curtain Air Bags</a>
                   </li>
                   <li>
                     <a href="">Knee Air Bags</a>
                   </li>
                   <li>
                     <a href="">Side Air Bags</a>
                   </li>
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Wheels & Tires</h6>
                 <ul>
                   <li>
                     <a href="">Premium Wheels 19”+</a>
                   </li>
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Steering</h6>
                 <ul>
                   <li>
                     <a href="">Power Steering</a>
                   </li>
                   <li>
                     <a href="">Tilt Steering</a>
                   </li>
                 </ul>
               </div>
             </div>
           </div>
         </div>
       </div>;              
            
   }
 };

export default DetailSpecification;