import React from "react";
import { Field, reduxForm } from "redux-form";
import Recaptcha from "react-recaptcha";

const required = value => (value ? undefined : "This field is required.");
const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ? "Invalid email address." : undefined;

const renderInput = ({
   
    input,
    label,
    type,
    meta: { touched, error }
}) => (
    <div className="form-group">
        <label className="control-label col-sm-12"><h6>{ label }</h6></label>
        <div className="col-sm-12">
                {(type == "text" || type == "email") ? <input { ...input }  className="form-control" type={ type } /> : <textarea { ...input } className="form-control" ></textarea> }
            { touched && ((error && <span className="contact-form-error-message"  style={{color:'red'}}>{ error }</span>)) }
        </div>
    </div>
);

const captcha = (props) => (
    <div className="form-group">
        <label className="col-sm-12 control-label"></label>
        <div className="col-sm-12">
            <Recaptcha render="explicit" onloadCallback={ console.log.bind(this, "reCAPTCHA loaded.") }
                sitekey="6LcaglYUAAAAAH0izdq4UPPMxRSAJZAGN52NMqHq" onChange={props.input.onChange} />
           
        </div>
    </div>
);

const SelectingFormValuesForm = props => {
    //console.log("props",props);
    const { handleSubmit, submitting } = props
    return (
        <form className="form-horizontal" onSubmit={ handleSubmit }>
            <Field
                name="name"
                type="text"
                component={ renderInput }
                label="Name:"
                validate={ required }
            />
            
            <Field
                name="email"
                type="email"
                component={ renderInput }
                label="Email:"
                validate={ [required, email] }
            />
            <Field
                name="phoneNo"
                type="text"
                component={ renderInput }
                label="Phone_No:"
                validate={ [required] }
            />
            <Field
                name="subject"
                type="text"
                component={ renderInput }
                label="Subject:"
                validate={ required }
            />
            <Field
                name="message"
                type="textarea"
                component={ renderInput }
                label="Message:"
                validate={ required }
            />
            <Field name="recaptchacode" component={captcha} />

            <div className="form-group">
              <label className="col-sm-2 control-label"></label>
              <div className="col-sm-10">
                  <button type="submit" id="contact-form-button" disabled={ submitting }>Send</button>
              </div>
            </div>
        </form>
    )
}

export default reduxForm({
    form: "SelectingFormValuesForm"
})(SelectingFormValuesForm);

