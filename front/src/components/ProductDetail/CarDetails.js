import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
const imgPath = "http://aygauto.com/backend/public/cars_images/";
class CarDetails extends Component { 
  constructor() {
    super();
    this.state = {
       open:'carousel slide',
       close:'fullscreen-close',
       arr:[],       
       i:1
    }
  }
  onSubmit=()=>{
   
    let data="carousel slide fullscreen";
    this.setState({open:data});
    let close ="fullscreen-close open";
    this.setState({ close: close });
  }
  onClose = () => {

    let data = "carousel slide";
    this.setState({ open: data });
    let close = "fullscreen-close";
    this.setState({ close: close });
  }
  other_information=(info)=>{
      let f=[];
      this.state.arr = this.props.carDetails.other_information; 
     _.mapKeys(this.state.arr, function (value, key) {
        if (key === info && value != null) {
         f= value.map((val, index) => {
           return <li key={index}><a>{val}</a></li>         
          })
        } 
      
      })
    if (_.isEmpty(f))
     {return <li><a>No Information Available</a></li> }
      else { return f;  }
   
  }

  video_url = (info)=>{
    let v = [];
    let i=1;
    this.state.arr = this.props.carDetails.other_information;
    _.mapKeys(this.state.arr, function (value, key) {
      
      if (key === info && value != null) {
        v = value.map((val, index) => {
           i = i +index;
          return (< div className="item carousel-item" data-slide-number={index + 1} key={index}>
            <iframe src={val} allow="autoplay; encrypted-media" allowFullScreen="" width="100%" height="100%" frameBorder="0"></iframe>
          </div >);
        })
      }

    })
    this.state.i = i;   
    if (_.isEmpty(v)) {
     
    }
    else {      
      return v;
    }
  }
    render() {     
      //console.log(this.props.carDetails);
        return (
        <div className="wap">
        <div className="details-box">
        <div className="left">
        <span>{this.props.carDetails.year}</span>
          <h4>
            {this.props.carDetails.brand} <span>  {this.props.carDetails.model_name} </span>
          </h4>
                <span>{this.props.carDetails.transmission}  </span>
        </div>
        <div className="save">
          <span className="icon">
           
          </span>
          
                <i className="fa fa-heart-o wishlist-icon" />
        </div>
        <div className="right">
                <span>{this.props.carDetails.miles}   miles </span>
          <h3>${this.props.carDetails.price}</h3>
        </div>
      </div>
      <div className="product-slider">
      
      <button className="fullscreen-icon" onClick={this.onSubmit} />
      <button className={this.state.close} onClick={this.onClose} />
        {/*<span className="fullscreen-icon" />
        <span className="fullscreen-close" />*/}
        <div id="productSlider" className={this.state.open}>
          <div className="carousel-inner">
            <div className="active item carousel-item" data-slide-number="0">
              <img src={`${imgPath}${this.props.carDetails.thumbnail}`} alt="" />
            </div>
                {/*  < div className="item carousel-item" data-slide-number="1" >
                 <iframe src="https://www.youtube.com/embed/Vh5ot31hEGg" allow="autoplay; encrypted-media" allowFullScreen="" width="100%" height="100%" frameBorder="0"></iframe>
                </div >
                */}
                {this.video_url('video_urls')}     
           
            { _.isArray(this.props.carDetails.slider_images) ? this.props.carDetails.slider_images.map((item, index) => (
            <div className="item carousel-item" data-slide-number={index+this.state.i} key={index}>
               <img src={`${imgPath}${item}`} alt=""/>
            </div>
            )):''}
            <a className="carousel-control left pt-3" href="#productSlider" data-slide="prev">
              <i className="fa fa-chevron-left fa-2x" />
            </a>
            <a className="carousel-control right pt-3" href="#productSlider" data-slide="next">
              <i className="fa fa-chevron-right fa-2x" />
            </a>
          </div>

          <ul className="carousel-indicators list-inline">
            <li className="list-inline-item active">
              <a id="carousel-selector-0" className="selected" data-slide-to="0" data-target="#productSlider">
                <img src={`${imgPath}${this.props.carDetails.thumbnail}`} className="img-fluid" alt=""/>
              </a>
            </li>
                  
                  {/*https://www.youtube.com/watch?v=qvE2miLMbNk*/}
                  { _.isArray(this.props.carDetails.slider_images) ? this.props.carDetails.slider_images.map((item, index) => (
                   <li className="list-inline-item" key={index}>
                   <a id="carousel-selector-1" data-slide-to={index+this.state.i} data-target="#productSlider">
                     <img src={`${imgPath}${item}`} alt="" alt="" />
                   </a>
                 </li>
            )):''}
           
            
           
          </ul>
        </div>
      </div>
      <div>
        
      </div>
      <div className="specs-details">
              <h3>&nbsp;&nbsp;&nbsp;&nbsp;</h3>   
         <h3 >Details & Specs</h3>
              
         <div className="specs-details-main">
           <div className="spotlight-box">
             <div className="img">
                    {/* <img src={imgPath} alt="" />*/}
              </div>
             <div className="mpg-details">
               <h5>MPG</h5>
               <div className="city">
                 {this.props.carDetails.mpg_city}
                 <span className="">City</span>
               </div>
               <div className="city">
               {this.props.carDetails.mpg_hwy}
                 <span className="text">HWY</span>
               </div>
             </div>
             <div className="transmission">
               <h5>Transmission</h5>
               <span> {this.props.carDetails.car_transmission} </span>
             </div>
             <div className="transmission">
               <h5>Drivetrain</h5>
               <span>{this.props.carDetails.drivetrain}</span>
             </div>
           </div>

           <div className="basics-main">
             <div className="basics-left">
               <h5>The Basics</h5>
               <table className="table table-striped">
                 <tbody>
                   <tr>
                     <td>Body Style</td>
                     <td>{this.props.carDetails.car_categories}</td>
                   </tr>
                   <tr>
                     <td>Passenger Capacity</td>
                     <td>{this.props.carDetails.passenger_capacity}</td>
                   </tr>
                   <tr>
                     <td>Exterior Color</td>
                     <td>{this.props.carDetails.car_color}</td>
                   </tr>
                   <tr>
                     <td>Wheel</td>
                     <td>{this.props.carDetails.wheel}</td>
                   </tr>
                   <tr>
                     <td>Tire</td>
                     <td>{this.props.carDetails.wheel}</td>
                   </tr>
                   {/*<tr>
                     <td>Basic Warranty</td>
                     <td>48 mo./50,000 mi</td>
                   </tr>*/}
                   <tr>
                     <td>PowerTrain Warranty</td>
                     <td>{this.props.carDetails.powertrain_warranty}</td>
                   </tr>
                   <tr>
                     <td>Engine</td>
                     <td>{this.props.carDetails.engine}</td>
                   </tr>
                   <tr>
                     <td>Horsepower</td>
                     <td>{this.props.carDetails.horsepower}</td>
                   </tr>
                   <tr>
                     <td>Transmition</td>
                     <td>{this.props.carDetails.transmission}</td>
                   </tr>
                   <tr>
                     <td>Stock Number</td>
                     <td>{this.props.carDetails.stock_number}</td>
                   </tr>
                   <tr>
                     <td>VIN</td>
                     <td>{this.props.carDetails.vin}</td>
                   </tr>
                 </tbody>
               </table>
             </div>
             <div className="advanded-right card-columns">
               <div className="advanded-box card">
                 <h6>Accessory Packages</h6>
                 <ul>
                        {this.other_information('accessory_packages')}
             
                 </ul>
               </div>
               <div className="advanded-box card">
                 <h6>Braking & Traction</h6>
                 <ul>
                        {this.other_information('braking_traction')}
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Comfort & Convenience</h6>
                 <ul>                  
                    { this.other_information('comfort_convenience')}                       
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Entertainment & Instrumentation</h6>
                 <ul>              
                    {this.other_information('entertainment_instrumentation')}
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Lighting</h6>
                 <ul>
                        {this.other_information('lighting')}
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Exterior</h6>
                 <ul>
                      {this.other_information('exterior')}
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Seats</h6>
                 <ul>
                        {this.other_information('seats')}
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Safety & Security</h6>
                 <ul>
                        {this.other_information('safety_security')}
                 </ul>
               </div>

               <div className="advanded-box card">
                 <h6>Wheels & Tires</h6>
                 <ul>
                        {this.other_information('wheels_tires')}
                 </ul>
               </div>
               <div className="advanded-box card">
                 <h6>Steering</h6>
                 <ul>
                      {this.other_information('steering')}
                 </ul>
               </div>
             </div>
           </div>
         </div>
       </div>
    </div>
    );
    }
}

export default connect()(CarDetails);