import React, { Component } from 'react'
import { Link } from "react-router-dom";

import HeaderLinks from '../Common/HeaderLinks'
import SocialLink from '../Common/SocialLink'
import logo from '../../assets/images/logo.svg'

const Header = () => (
	<header className="navbar navbar-toggleable-md navbar-light fixed-top">
		<button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#Topnav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span className="navbar-toggler-icon"></span>
		</button>
        <Link to="/" className="navbar-brand">
            <img src={logo} alt="logo" />
        </Link>
		<div className="collapse navbar-collapse" id="Topnav">
            <HeaderLinks />
            <SocialLink place="header"/>  
		</div>
	</header>  
);

export default Header;