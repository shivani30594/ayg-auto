import React, { Component } from 'react'

import HeaderLinks from '../Common/HeaderLinks'
import SocialLink from '../Common/SocialLink'
import FooterCopyright from './FooterCopyright'

const Footer = () => (
    <footer>
        <div className="container">
            <div className="row">
                <div className="col-md-10 col-12">
                     <HeaderLinks />
                </div>
                <div className="col-md-2 col-12">
                    <SocialLink place="footer"/>
                </div>
            </div>
        </div>
        <FooterCopyright /> 
    </footer>
)

export default Footer