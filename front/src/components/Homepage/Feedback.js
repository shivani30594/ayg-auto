import React, { Component } from 'react'
import carApi from "../../api/carApi";
import FeedbackImg from '../../assets/images/feedback-img.jpg';

import Customer1 from '../../assets/images/customer2.jpg'
import Customer from '../../assets/images/Customer.jpg'
import _ from "lodash";
const imagePath = 'http://aygauto.com/backend/public/testimonial_images/';
//const imagePath = 'http://localhost/ali/ayg-auto/backend/public/testimonial_images/';
const image = 'http://aygauto.com/backend/public/testimonial_images/';
class Feedback extends Component {
	state = { current: 1, total: '', data: [], index: 1 }
	componentDidMount() {
		carApi.getTestimonial().then((response => {
			this.setState({ data: response });
		}));
	}
	render() {
		return <section className="feedback-bg">
			<div className="container">
				<div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
					<div className="carousel-inner" role="listbox">
						{this.state.data.map((item, index) => (

							<div className={`carousel-item
							 ${index === 0 ? 'active' : ''}`} key={index}>
								<div className="row" key={index}>
									<div className="col-md-5 col-12">
										<div className="img">
											<img className="d-block img-fluid" src={`${imagePath}${item.image}`} alt="" />
										</div>
									</div>
									{/*style={{ height: '202.483px', width: '100%' }}*/}
									<div className="col-md-7 col-12">
										<h3>{item.title}</h3>
										<p>{item.description}</p>

									</div>
								</div>
							</div>
						))}					
						
					</div>
					<a className="carousel-control-prev" href="#carouselExampleControls" id="carouselExampleControls" role="button" data-slide="prev">
						<span className="fa fa-angle-left" aria-hidden="true" />
						<span className="sr-only">Previous</span>
					</a>
					<a className="carousel-control-next" href="#carouselExampleControls" id="carouselExampleControls" role="button" data-slide="next">
						<span className="fa fa-angle-right" aria-hidden="true" />
						<span className="sr-only">Next</span>
					</a>
				</div>
			</div>
		</section>;
	}
};
export default Feedback;