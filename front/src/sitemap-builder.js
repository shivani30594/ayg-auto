import router from './App'
import siteMapBuilder, { getSites } from 'react-router-sitemap-builder'


getSites(router);

 [ '/',
  '/cars',
  '/car-details/:id',
 ]
 //console.log(router);
/* router, prefix, save_to_where */
sitemapBuilder(router, 'http://localhost:3000/', __dirname + '/sitemap.txt');