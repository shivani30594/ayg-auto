import React, { Component } from 'react';
//import Select from "react-select";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
//import carApi from "../api/carApi";
import _ from 'lodash';
import { carsRequest, brandRequest, modelRequest, carFliterCarsRequest} from "../actions/carsActions";
//import Price from '../components/Productlist/RangeSlider/Price';
//import Year from '../components/Productlist/RangeSlider/Year';
//import Mileage from '../components/Productlist/RangeSlider/Mileage';
import BodyStyle from '../components/Productlist/BodyStyle';
import InputRange from "react-input-range";
//import Filters from '../components/Productlist/Filters';
import Productlist from '../components/Productlist/Productlist';
import 'react-input-range/lib/css/index.css';
const styles = {
    color: 'blue'
};
class ProductListing extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data:[],
            arr:[],
            value5: { min: 100, max: 100000 },
            year: { min: 2003, max: 2018 },
            mileage: { min: 100, max: 500000 },
            bodyStyle:"",
            option: [{ model: "Model3", name: "Model3", age: 50 }, { model: "Model4", name: "Model4", age: 30 }, { model: "Model5", name: "Model5", age: 30 }, { model: "Model6", name: "Model6", age: 30 }]
        }

    }

    componentDidMount() {
       
        this.carDetails();
        if (this.props.carsData.selectedData.price_car !== null && this.props.carsData.selectedData.price_car !=='')
        {
            if(this.props.carsData.selectedData.price_car === "15000_25000")
            {
                this.state.value5.min=15000;
                
                this.state.value5.max=25000;
            }
            else if (this.props.carsData.selectedData.price_car === "25000_35000") {
                this.state.value5.min = 25000;
                this.state.value5.max = 35000;
            }
            else if (this.props.carsData.selectedData.price_car === "35000") {
                this.state.value5.min = 35000;             
            }
            else if (this.props.carsData.selectedData.price_car === "5000")
            {
                this.state.value5.min = 5000;
            }
            
        }
        if (this.props.carsData.selectedData.select_year != null && this.props.carsData.selectedData.select_year !='') {
            this.state.year.min = this.props.carsData.selectedData.select_year;
            
        }
       
        if (this.props.carsData.selectedData.car_categories != null && this.props.carsData.selectedData.car_categories !='') {

            this.setState({ bodyStyle: this.props.carsData.selectedData.car_categories });
        }
    }
    
  

    carDetails()
    {
        
        if (this.props.carsData.carsFliterData != null) 
        {
            this.state.data = this.props.carsData.carsFliterData;
        }
        else
        {
        
        this.props.carsRequest().then(response=>{
            this.setState({ data: this.props.carsData.carsData });
        });
        }
        if (this.props.carsData.brand != null) {
            this.props.brandRequest();
        }  
       
        
        
    }
    onChangeComplete(value) {
        // {value => console.log(value)}
        this.setState({ value5: value });
        
    }
    onChangeComplete1(value) {
        // {value => console.log(value)}
        this.setState({ year: value });
       
    }
    onChangeComplete2(value) {
        // {value => console.log(value)}
        this.setState({ mileage: value });
        
    }


  handleChange = selected => {
        this.setState({ selectedOptions: selected });
    };


    change = (event) => {        

        this.props.carFliterCarsRequest("brand", event.target.value).then(response => {
            this.setState({ data: this.props.carsData.filterData });
        });
        this.props.modelRequest(event.target.value);
    }

    changemodel = (event) => {
        this.props.carFliterCarsRequest("model_name", event.target.value).then(response => {
            this.setState({ data: this.props.carsData.filterData });
        });
             
    }

    renderSelectOptions = (value,brand) => {
        
        if(brand === value)
        {                   
            return(<option key={value}  value={value} selected >{value}</option>);
        }
        else
        {
            return(<option key={value}  value={value}>{value}</option>);
        }
       
    }


    handleOptionChange = (changeEvent) => {
       //alert(changeEvent.target.value);
       /* let a = _.findIndex(this.state.data, function (o) { return o.car_color == changeEvent.target.value; });
        if(a>0)
        {
            this.state.data = _.values(this.state.data[a]);

           // this.setState({ data:this.state.arr});           
             
            console.log("Datadsfgdgcfbh", _.isArray(this.state.data));
            console.log('sdfsffsdfdfs',this.state.data);

        }*/
       
        let method ="";
            if (this.state.bodyStyle != '')
            {
                method = "bodyStyle="+this.state.bodyStyle+"&";
            }
            method = method+"car_color="+changeEvent.target.value;
            
         this.props.carFliterCarsRequest("car_color",changeEvent.target.value).then(response => {           
         this.setState({ data:this.props.carsData.filterData});
        });
}


    handleOptionBodyStyle = (changeEvent) => {
        this.setState({ bodyStyle: changeEvent.target.value });
       // alert(changeEvent.target.value);
        this.props.carFliterCarsRequest("car_categories",changeEvent.target.value).then(response => {
            this.setState({ data: this.props.carsData.filterData });
        });
    }
    handleOptionTransmissionChange=(changeEvent)=>{
        
        this.props.carFliterCarsRequest("car_transmission",changeEvent.target.value).then(response => {
            this.setState({ data: this.props.carsData.filterData });
        });        
    }
    resetFilter=()=>{
        window.location.reload();        
    }
    render() {
        const { selectedOptions } = this.state;
       
        return (
            <div>
                <section className="productlist-bg">
                    <div className="row no-gutters">
                        <div className="col-xl-3 col-12">
                            <a href="javascript:void(0)" className="filtering-button fa fa-plus hidden-xl-up"></a>
                            <div className="filtering-main">
                                <BodyStyle bodyStyle={this.state.bodyStyle}
                                    handleOptionBodyStyle={this.handleOptionBodyStyle}/>
                                <div className="filtering-box">
                                    <h4>Make</h4>
                                   
                                    <div className="form-group select">
                                        <select id="select" name="car_brand" className="form-control" onChange={event => this.change(event)}>
                                            <option value="">Select Brand</option>
                                            {_.isArray(this.props.carsData.brand) ? this.props.carsData.brand.map((value, index) => (this.renderSelectOptions(value,this.props.carsData.selectedData.car_brand))) : [].map(this.renderSelectOptions)}
                                        </select>
                                    </div>
                                   
                                </div>
                                <div className="filtering-box">
                                    <h4>Model</h4>
                                    <div className="form-group select">
                                        <select id="select" name="car_model" className="form-control" onChange={event => this.changemodel(event)}>
                                            <option value="">Select Model</option>
                                            {_.isArray(this.props.carsData.models) ? this.props.carsData.models.map((value, index) => (this.renderSelectOptions(value,this.props.carsData.selectedData.car_model))) : [].map(this.renderSelectOptions)}
                                        </select>
                                    </div>
                                   {/* <Select name="form-field-name" multi={true} value={selectedOptions} valueKey="model" labelKey="name" onChange={this.handleChange} options={this.state.option} />*/}
                                </div>
                                <div className="filtering-box">
                                    <h4>Price</h4>
                                    <div className="price-range">
                                        <form className="form">
                                            <label>
                                                <span></span>
                                                <span className="float-right" />
                                            </label>
                                            <InputRange
                                                draggableTrack
                                                maxValue={100000}
                                                minValue={100}
                                                onChange={value => this.setState({ value5: value })}
                                                onChangeComplete={this.onChangeComplete.bind(this)}
                                                value={this.state.value5}
                                            />
                                        </form>
                                    </div>
                                </div>
                                <div className="filtering-box">
                                    <h4>Year</h4>
                                    <div className="year-range">
                                        <label>
                                            <span style={styles}></span>
                                            <span className="float-right" style={styles}></span>
                                        </label>
                                        <InputRange draggableTrack maxValue={2018} minValue={2003} onChange={value => this.setState(
                                            { year: value }
                                        )} onChangeComplete1={this.onChangeComplete1.bind(this)} value={this.state.year} />
                                    </div>
                                </div>
                                <div className="filtering-box">
                                    <h4>Mileage</h4>
                                    <div className="mileage-range">
                                        <label>
                                            <span> </span>
                                            <span className="float-right" />
                                        </label>
                                        <InputRange
                                            draggableTrack                                            
                                            maxValue={500000}
                                            minValue={100}
                                            onChange={value => this.setState({ mileage: value })}
                                            onChangeComplete2={this.onChangeComplete2.bind(this)}
                                            value={this.state.mileage}
                                        />
                                    </div>
                                </div>
                                <div className="filtering-box">
                                    <h4>Colors</h4>
                                    <div className="colors-main">
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color" value="Black" onChange={this.handleOptionChange.bind(this)}/>
                                            <label htmlFor="color" className="black"></label>
                                        </div>
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color2" value="Grey" onChange={this.handleOptionChange.bind(this)}/>
                                            <label htmlFor="color2" className="grey"></label>
                                        </div>
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color3" value="Grey-light" onChange={this.handleOptionChange.bind(this)}/>
                                            <label htmlFor="color3" className="grey-light"></label>
                                        </div>
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color4" value="White" onChange={this.handleOptionChange.bind(this)}/>
                                            <label htmlFor="color4" className="white"></label>
                                        </div>
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color5" value="Red" onChange={this.handleOptionChange.bind(this)} />
                                            <label htmlFor="color5" className="red"></label>
                                        </div>
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color6" value="Blue" onChange={this.handleOptionChange.bind(this)}/>
                                            <label htmlFor="color6" className="blue"></label>
                                        </div>
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color7" value="Silver" onChange={this.handleOptionChange.bind(this)} />
                                            <label htmlFor="color7" className="silver"></label>
                                        </div>
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color7" value="Orange" onChange={this.handleOptionChange.bind(this)}/>
                                            <label htmlFor="color7" className="orange"></label>
                                        </div>
                                        <div className="color-box">
                                            <input type="radio" name="color" id="color8" value="Brown" onChange={this.handleOptionChange.bind(this)}/>
                                            <label htmlFor="color8" className="brown"></label>
                                        </div>
                                    </div>
                                </div>
                                <div className="filtering-box">
                                    <h4>Transmission</h4>
                                    
                                    <div className="btn btn-secondary btn-transm active">
                                            <input type="radio" style={{display:"none"}} name="Transmission" id="Automatic" value="Automatic" onChange={this.handleOptionTransmissionChange.bind(this)}/>
                                            <label htmlFor="Automatic" >Automatic</label>
                                    </div>
                                    <div className="btn btn-secondary btn-transm ">
                                            <input type="radio" style={{display:"none"}} name="Transmission" id="Manual" value="Manual" onChange={this.handleOptionTransmissionChange.bind(this)}/>
                                            <label htmlFor="Manual">Manual</label>
                                    </div>
                                </div>
                                <div className="filtering-box">
                                   
                                    <button className="btn btn-secondary btn-block" onClick={this.resetFilter} >Reset Filters</button>
                                    <div className="btn btn-secondary btn-block active">Save Search</div>
                                 </div>
                            </div>
                        </div>
                        <Productlist carsData={this.state.data} data={this.state.value5} year={this.state.year} mileage={this.state.mileage}  />
                    </div>
                </section>
            </div>
        );
    }
}
function mapStateToProps(state) {
    //console.log(state);
    return {carsData:state.car}
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(carsRequest, dispatch)
    }
}
export default connect(mapStateToProps, { brandRequest, carFliterCarsRequest,carsRequest,modelRequest})(ProductListing);