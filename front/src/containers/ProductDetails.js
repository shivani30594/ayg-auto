import React, {Component} from 'react';
import {connect} from "react-redux";
//import {bindActionCreators} from "redux";

import carApi from "../api/carApi";
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import CarSpecification from "../components/ProductDetail/CarDetails";
import {carDetailsRequest} from "../actions/carsActions";
import  FormValue from "../components/ProductDetail/SelectingFormValuesForm";
//import Loader from 'react-loaders'
import _ from "lodash";
import Modal from "react-responsive-modal";
//var Loader = require('react-loaders').Loader;
const styles = {
    fontFamily: "sans-serif",
    textAlign: "center"
};
class Productdetails extends Component {  
    constructor(props)
    {
        super()
        this.state={
            open:false,
            header:" "
        }
    }
    componentDidMount() {       
        this.props.carDetailsRequest(this.props.match.params.id);
    }
    renderLoader() {
       
        return <div className="loader-wapper"><div className="loader"></div> </div>
    }
    onCloseModal = () => {
        this.setState({ open: false });
    };

    contactForm = (h) => {
        this.setState({ open: true });
        this.setState({ header: h });
    };

    onSubmit = (values) => { 
       
        let arr = { "brand": this.props.carDetails.carDetails.brand, "model_name": this.props.carDetails.carDetails.model_name, "link": window.location.href };
               
        carApi.sentMail(values,arr).then(response => {
            NotificationManager.success('Your Mail has Successfully Sent ', 'Sent Mail');
        });       
        this.setState({ open: false }); 
    }
    render() {
       
        return (            
            <section className="productdetails-bg">
                {this.props.carDetails.isFetching === true ? this.renderLoader() : ''
                }
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <CarSpecification 
                                carDetails={this.props.carDetails.carDetails}
                                />
                            <div className="conciergecta-bg">
                                <h4>We will bring the car to you!</h4>
                               
                                <p>
                                    Schedule a concierge appointment and we will drive the car to you.
                                </p>
                               
                                <button type="submit" className="btn btn-info" onClick={this.contactForm.bind(this, "Schedule Now")}> Schedule Now</button>
                                <div className="model" style={styles}>
                                <Modal className="schedule-model" open={this.state.open} onClose={this.onCloseModal.bind(this)} >
                                <h2>{this.state.header}</h2>
                                <FormValue onSubmit={this.onSubmit}/>
                                </Modal>
                                </div>
                            </div>

                            <div className="financingcta-bg">
                                <h4>Buy from your couch</h4>
                                <p>
                                    Complete a short, two-minute form and get terms instantly.
                                </p>
                                <button type="submit" className="btn btn-info" onClick={this.contactForm.bind(this, "Get Pre-Qualified")}>Get Pre-Qualified</button>
                                                                 
                               
                           
                             </div>
                            <NotificationContainer />  
                    </div>
                </div>
                </div>
            </section>
        )
    }
};
function mapStateToProps(state) {
    return {carDetails:state.car}
}


export default connect(mapStateToProps, {carDetailsRequest})(Productdetails);