import * as apiConstant from './apiConstant'

class seoApi {
    static getSeo() {
        return fetch(`${apiConstant.API_URL}get-seo`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }

    static getCarSeo() {
        return fetch(`${apiConstant.API_URL}car_seo_by_id`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }
    // static getCarSeo(id) {
    //     return fetch(`${apiConstant.API_URL}car_seo_by_id?id=` + id, {
    //         method: 'get',
    //     }).then(function (response) {
    //         return response.json();
    //     }).catch(function (error) {
    //         throw error;
    //     });
    // }
}
export default seoApi