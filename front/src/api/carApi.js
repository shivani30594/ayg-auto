import * as apiConstant from './apiConstant'

class carApi {
    static getBrands(){
        return fetch(`${apiConstant.API_URL}get-brands-na`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }
    static getTestimonial() {
        return fetch(`${apiConstant.API_URL}get-all-testimonials`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }
    static getBrandModels(brand){
        //console.log(brand);
        return fetch(`${apiConstant.API_URL}get-brand-models-na?brand=`+brand, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }
    static getCarsAll(){
        return fetch(`${apiConstant.API_URL}get-cars-listing-na?`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }
    static getCarDetails(id){
        return fetch(`${apiConstant.API_URL}get-car-na?id=`+id, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }
    static getCarFliterHome(data){
        return fetch(`${apiConstant.API_URL}get-cars-home-fliter`, {
            method: 'post',
            body: data
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
                throw error;
        });
    }

    static getCarFilter(key,value) {
        
        let a= 'car_color = Black & brand=ford';
        return fetch(`${apiConstant.API_URL}get-cars-listing-na?${key}=${value}`, {
            method: 'get',           
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }


    static sentMail(response,carinfo){
        var data = new FormData();
        data.append('name',response.name);
        data.append('email',response.email);
        data.append('phoneNo',response.phoneNo);
        data.append('subject',response.subject);
        data.append('message',response.message);
       
        data.append('brand', carinfo.brand);
        data.append('model_name', carinfo.model_name);
        data.append('link',carinfo.link)
        //console.log(carinfo.brand + carinfo.model_name)
               
       return fetch(`${apiConstant.API_URL}sent-mail`, {
            method: 'post',
            body: data
        }).then(function (response) {
            return ("Done");
        }).catch(function (error) {
                throw error;
        });
    }
}
export default carApi