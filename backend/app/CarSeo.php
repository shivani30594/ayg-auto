<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarSeo extends Model
{
    protected $table = 'seo';
    // fillable
    protected $fillable = ['id','route','meta_title','meta_description','keyword'];

}
