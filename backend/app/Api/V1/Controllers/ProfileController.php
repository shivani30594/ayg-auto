<?php

namespace App\Api\V1\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JWTAuth;
use App\User;
use Dingo\Api\Routing\Helpers;

class ProfileController extends Controller {
	use Helpers;

	public function update(Request $request)
	{
	    $currentUser = JWTAuth::parseToken()->authenticate();

		$user = Admin::where('email', '=', $request->get('email'))->first();	    
		dd($user);

	    if(!$user)
	        throw new NotFoundHttpException;

	    $user->fill($request->all());

	    if($user->save())
	        return response()->json([	        	
	            'status' => 'ok',
	            'message' => 'Profile updated successfully.'	            
	        ]);
	    else
	        return response()->json([	        	
	            'status' => 'error'
	        ]);
	}	
}