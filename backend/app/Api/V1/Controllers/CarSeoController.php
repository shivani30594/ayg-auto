<?php

namespace App\Api\V1\Controllers;

use App\CarSeo;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Http\Request;
use JWTAuth;

class CarSeoController extends Controller
{    
    
 /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function getSeo_by_id($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $carseo = CarSeo::findOrFail($id);
            return response()->json($carseo);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }
    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSeo()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser) {
            $carseo = CarSeo::all();
            return response()->json($carseo);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }

       /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function editSeo(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        if ($currentUser) {
            $id = $request->get('id');           
            $carseo = CarSeo::findOrFail($id);

            $carseo->fill($request->all());
            // $carseo->route             = $request->get('route');
             //$carseo->meta_title        = $request->get('meta_title');
             //$carseo->meta_description  = $request->get('meta_description');
             //$carseo->keyword           = $request->get('keyword');

           $carseo->save();

            return response()->json([
                'status'   => 'success',
                'data'     => $carseo,
                'message'  => 'CarSeo Successfuly Updated',
            ]);
        } else {
            return response()->json([
                'status'  => 'ERROR',
                'message' => 'AUTH ISSUES || H$R#EN',
            ]);
        }

    }
/**
 * [getSeo description]
 * @return [type] [description]
 */
    public function getSeoAll()
    {
        $carseo = CarSeo::all();
            if($carseo)
            {
                 return response()->json($carseo);
            }
            else
            {
                return response()->json([
                    'status' => 'ERROR',
                    'message' => 'AUTH ISSUES || H$R#EN',
                ]);
            }

    }
    
}
