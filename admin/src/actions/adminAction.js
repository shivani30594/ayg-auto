import * as types from './actionTypes';
import adminApi from '../api/adminApi'

export function adminRequestInit(){
    return { type: types.ADMIN_ACCOUNT_REQUEST }
}

export function adminRequestSuccess(payload){
    return {type: types.ADMIN_ACCOUNT_SUCCESS, payload}
}

export function adminRequest(){
    return function(dispatch) {
        dispatch(adminRequestInit())
        return adminApi.fetchUser().then(response => {
            dispatch(adminRequestSuccess(response)) 
        }).catch(error => {
            throw error 
        })
    }
}