import React, { Component } from "react";

import loader from "../../assets/img/loading-bubbles.svg";
const Loader = () => (
    <div style={{textAlign:'center', backgroundColor:'#1DC7EA'}}>
        <img src={loader} alt="Loading"/>
    </div>
)

export default Loader; 