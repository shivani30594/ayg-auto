import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import {Field, reduxForm, initialize} from "redux-form";

class DocumentInput extends React.Component {
  render() {
    var indexValue = this.props.index.toString();
    return <Field
      id={`youtube_url_${indexValue}`}
      name={`youtube_url_${indexValue}.`}
      type="text"
      placeholder="Add YouTube URL For Video"
      className="form-control"
      />;
  }
}

class TextboxD extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      documents: []
    }

    this.add = this
      .add
      .bind(this);
  }

  add() {
    const documents = this
      .state
      .documents
      .concat(DocumentInput);
    this.setState({documents});
  }

  render() {
    const documents = this
      .state
      .documents
      .map((Element, index) => {
        return <Element key={index} index={index}/>
      });

    return <div className="form-group">
  

      <div className="inputs">
        {documents}
      </div>
    </div>
  }
}
export default TextboxD;