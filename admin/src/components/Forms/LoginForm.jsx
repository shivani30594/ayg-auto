import React, {Component} from 'react';
import {Field, reduxForm} from "redux-form";
import {Link} from "react-router-dom";
import {Alert} from "react-bootstrap";
const required = value => value
    ? undefined
    : 'This Field Is Required.'
const email = value => value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Please Enter Valid Email Address'
    : undefined
const renderField = ({
    input,
    label,
    type,
    meta: {
        touched,
        error,
        warning
    }
}) => (
    <div className="form-group">
        <label className="control-label">{label}</label>
        <span className="required">*</span>

        <input {...input} placeholder={label} type={type} className="form-control"/> {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
)

const LoginForm = (props) => {
    const {handleSubmit, pristine, reset, submitting} = props
    return (
        <form method="post" onSubmit={handleSubmit(props.onSubmit)}>
                                        <div className="card">
                                            <div className="header text-center">
                                                <h4 className="title">Login</h4>
                                                <p className="category"></p>
                                            </div>
                                            <div className="content">
                                                <div>
                                                    <Field name="username" type="email"
                                                        component={renderField} label="Email"
                                                        validate={[email, required]}
                                                    />
                                                    <Field name="password" type="password"
                                                        component={renderField} label="Password"
                                                        validate={[required]}
                                                    />
                                                    <div className="footer text-center">
                                                        <div className="legend">
                                                            <button type="submit" className="btn-fill btn-wd btn btn-info {return props.sessiondata.loggingIn ? 'disabled': '' }" >
                                                                Submit
                                                            </button>
                                                        </div>
                                                    </div>
                                                    {props.sessiondata.error ?
                                                        <Alert bsStyle="danger">             <span>
                                                            {props.sessiondata.error}</span>
                                                        </Alert>
                                                        : ''}
                                                </div>
                                            </div>
                                        </div>
                                    </form>
    )
}

export default reduxForm({
    form: 'loginForm' // a unique identifier for this form
})(LoginForm)