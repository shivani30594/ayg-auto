import React, { Component } from 'react';
import CKEditor from "react-ckeditor-component";
 
class Ckeditor extends Component {
    constructor(props) {
        super(props);
        this.updateContent = this.updateContent.bind(this);
        this.state = {
            content: 'content',
        }
    }
 
    updateContent = (newContent) => {
        this.setState({
            content: newContent
        })
    }
        
    onBlur = (evt)=>{
      var newContent = evt.editor.getData()      
      this.setState({
        content: newContent
      })
      this.props.input.onChange(newContent)
      //this.props.contentChange(newContent)      
    }
    
    afterPaste= (evt) => {
      console.log("afterPaste event called with event info: ", evt);
    }
 
    render() {
       // console.log('thee',this.props)
        return (
            <CKEditor 
              name="content"
              activeClass="p10" 
              content={this.state.content} 
              events={{
                "blur": this.onBlur,
                "afterPaste": this.afterPaste,
              }}
             />
        )
    }
}
export default Ckeditor;