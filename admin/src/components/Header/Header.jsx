import React, { Component } from 'react';
import { Navbar } from 'react-bootstrap';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import HeaderLinks from './HeaderLinks.jsx';

import appRoutes from 'routes/app.jsx';
import * as sessionAction from "../../actions/sessionAction";
class Header extends Component{
    constructor(props){
        super(props);
        this.mobileSidebarToggle = this.mobileSidebarToggle.bind(this);
        this.state = {
            sidebarExists: false
        };
    }
    onLogout = () => {
        this.props.actions.logOutUser();
    }
    
    mobileSidebarToggle(e){
        if(this.state.sidebarExists === false){
            this.setState({
                sidebarExists : true
            });

        }
        e.preventDefault();
        document.documentElement.classList.toggle('nav-open');
        var node = document.createElement('div');
        node.id = 'bodyClick';
        node.onclick = function(){
            this.parentElement.removeChild(this);
            document.documentElement.classList.toggle('nav-open');
        };
        document.body.appendChild(node);
    }
    getBrand(){
        var name;
        appRoutes.map((prop,key) => {
            if(prop.collapse){
                 prop.views.map((prop,key) => {
                    if(prop.path === this.props.location.pathname){
                        name = prop.name;
                    }
                    return null;
                })
            } else {
                if(prop.redirect){
                    if(prop.path === this.props.location.pathname){
                        name = prop.name;
                    }
                }else{
                    if(prop.path === this.props.location.pathname){
                        name = prop.name;
                    }
                }
            }
            return null;
        })
        return name;
    }
    render(){
        //console.log(this.props);
        return (
            <Navbar fluid>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#pablo">{this.getBrand()}</a>
                    </Navbar.Brand>
                    <Navbar.Toggle onClick={this.mobileSidebarToggle}/>
                </Navbar.Header>
                <Navbar.Collapse>
                    <HeaderLinks onlogout={this.onLogout} />
                </Navbar.Collapse>
            </Navbar>
        );
    }
}
function mapDispatchToProps(dispatch){
    return {
        actions: bindActionCreators(sessionAction, dispatch)
    }
}

export default connect(null, mapDispatchToProps)(Header);