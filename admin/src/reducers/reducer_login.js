import {
    FETCH_LOGIN
} from "../actions";

export default function (state = {
    logged: false
}, action) {
    switch (action.type) {
        case FETCH_LOGIN:
            return {
                logged : true
            }
        default:
            return state;
    }
}