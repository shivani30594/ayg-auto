export default {
    session : !!sessionStorage.jwt,
    admin : {
        isFetching: true,
        user: {}
    },
    cars: {
        isFetching: false,
        addCar:{},
        carsStocks:{},
        carAvailable:{},
        carUpdate:{}
    },
    carsCategory:{
        isFetching:false,
        addCarCategory:{}
    }
}
