import { combineReducers } from "redux";
import { reducer as formReducers } from "redux-form";
import sessionReducer from "./sessionReducer";
import adminReducer from "./adminReducer"
import carReducer from "./carReducer";
import notificationReducer from './noticationReducer'
import categoryReducer from './categoryReducer'
const rootReducer = combineReducers({
    session: sessionReducer,
    form: formReducers,
    admin: adminReducer, 
    cars: carReducer,
    notification: notificationReducer,
    categoryReducer:categoryReducer


})

export default rootReducer;