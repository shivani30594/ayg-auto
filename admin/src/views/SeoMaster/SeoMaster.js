import React, { Component } from 'react';
import { render } from "react-dom";
import { Grid, Row, Col } from 'react-bootstrap';

import Loader from "../../components/Loader/Loader";
import { Card } from 'components/Card/Card.jsx';
import _ from 'lodash';
import { Link } from "react-router-dom";
// Import React Table
import ReactTable from "react-table";
import "../../../node_modules/react-table/react-table.css";
import { connect } from "react-redux";
import carApi from "../../api/carApi";
import seoApi from "../../api/seoApi";
import { deleteCarsCategory } from "../../actions/carCategoryAction";
const imagePath = 'http://aygauto.com/backend/public/ayg_images/';

class SeoMaster extends React.Component {
    constructor() {

        super();
        this.state = {
            data: []
        };
    }

    componentDidMount() {
        this.fetchdata();
           }
    fetchdata() {
        seoApi
            .getSeo()
            .then(response => {

                this.setState({ data: response })
            })
    }
    render() {
        
        const { data } = this.state;
        return (
            <Grid fluid>
                <Card
                    title="Seo Cars "
                    content={
                
                <Row>
                
                    <Col md={12}>
                        <div style={{ 'text-align': "center" }}>
                            <ReactTable
                                data={data}
                                columns={[{
                                    columns: [
                                        {
                                            Header: "Rout  Name",
                                            accessor: 'route'
                                        }, {
                                            Header: "Meta Title",
                                            accessor: "meta_title",
                                           
                                        }, {
                                            Header: "Meta Description",
                                            accessor: "meta_description"
                                        }, {
                                            Header: "Keyword",
                                            accessor: "keyword"
                                        }, {
                                            Header: "Action",
                                            id: "action",
                                            Cell: row => (
                                                <div className="actionWapper">
                                                    <button
                                                        type="button"
                                                        className="btn btn-info btn-fill" >
                                                        <Link to={`/seo-edit/${row.original.id}`}>
                                                            <i className="pe-7s-pen"> </i> Edit</Link>
                                                    </button>
                                                   
                                                </div>
                                            )
                                        }
                                    ]
                                }
                                ]}
                                defaultPageSize={10}
                                className="-striped -highlight" />
                            <br />
                        </div>
                    </Col>
                </Row>
            }></Card>
            </Grid>
        )
    }
}
function mapStateToProps({ carsCategory }) {
    return { carsCategory }
}
export default connect(mapStateToProps, { deleteCarsCategory })(SeoMaster)
//export default SeoMaster
