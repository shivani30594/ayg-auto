import React, {Component} from 'react';
import {render} from "react-dom";
import {Grid, Row, Col} from 'react-bootstrap';

import Loader from "../../components/Loader/Loader";
import {Card} from 'components/Card/Card.jsx';
import _ from 'lodash';

// Import React Table
import ReactTable from "react-table";
import "../../../node_modules/react-table/react-table.css";
import carApi from "../../api/carApi";
import Modal from "react-responsive-modal";



import { connect } from "react-redux"
import { bindActionCreators } from "redux";
import { carsStocks,carAvailablesStocks,carUpdateStocks} from "../../actions/carsAction";


const imagePath = 'http://aygauto.com/backend/public/cars_images/';


const styles = {
    fontFamily: "sans-serif",
    textAlign: "center"
};
class CarsMaster extends Component {
   
    constructor() {

        super();
        this.state = {
            data: [],
            open: false,
            stocks:[],
            id:'',
            selectedOption:"Y"
        };
    }
    componentDidMount() {
       this.mountdata();
     

    }

 mountdata()
 {
     carApi
         .getCarsStocks()
         .then(response => {
            
             this.setState({ data: response })
         });
     
     this.props.carAvailablesStocks();

 }   

handleOptionChange(changeEvent) {
  
    this.setState({selectedOption:changeEvent.target.value });
}





    getCar_by_id(id,avaialable) {

       
        this.setState({selectedOption:avaialable});
        this.setState({id:id});
        this.setState({ open: true });      

    };

    
    onCloseModal = () => {
        this.setState({ open: false });
    };

    handleClick(req)
    {    
       
        var req = { 'car_id': this.state.id,'available':this.state.selectedOption};     
        this.props.carUpdateStocks(req,() => {
            this.mountdata();
            this.props.history.push('/inventory');
        }
        );
        this.setState({ open: false });
    }



    render() {
        const { data } = this.state;
        
        return (
            <div className="wrapper">
            <div style={styles}>
                <Modal open={this.state.open} onClose={this.onCloseModal.bind(this)} little>
                 <div><h2>Inventory Information</h2></div>
                    <table className="table-bordered">
                        <tbody> 

                                <div >
                                    <div className="row">
                                        <div className="col-sm-12">

                                            <form id="stockCar" >
                                                <div >
                                                    <label>
                                                        <input type="radio" name="stock" value="Y" checked={this.state.selectedOption === 'Y'} onChange={this.handleOptionChange.bind(this)} />
                                                       &nbsp;&nbsp;&nbsp;Stock
                                                    </label>
                                                </div>
                                                <div >
                                                    <label>
                                                        <input type="radio" name="stock" value="N" checked={this.state.selectedOption === 'N'} onChange={this.handleOptionChange.bind(this)} />
                                                         &nbsp;&nbsp;&nbsp;&nbsp;Out Of Stock
                                                 </label>
                                                </div>
                                                <input
                                                    className="btn btn-primary"
                                                    type="button"
                                                    value="Save"
                                                    onClick={this.handleClick.bind(this)}
                                                />
                                               
                                            </form>

                                        </div>
                                    </div>
                                </div>


                        
                        </tbody>
                    </table>
                </Modal>
            </div>
            <Grid fluid>
                <div className="content">
                    
                        <Card
                            title="Inventory"
                            content={
                        <Row>
                                    <Col md={12}>
                                        <div className="mb-20">
                                            <Col md={4}>
                                                <div className="font-icon-detail">
                                                    <span>Total Available Car <b>{this.props.stocks.instock} </b></span>
                                                </div>
                                            </Col>
                                            <Col md={4}>
                                                <div className="font-icon-detail">
                                                    <span>Total Not Available Car <b> {this.props.stocks.outstock}</b></span>
                                                </div>
                                            </Col>
                                        </div>
                                    </Col>
                        <Col md={12}>
                                <div style={{'textAlign':"center"}}>
                                <ReactTable
                                    data={data}
                                    columns={[{
                                        columns: [
                                            {
                                                Header: "Category Name",
                                                accessor: "car_categories"
                                            }, {
                                                Header: "Brand",
                                                accessor: "brand"
                                            }, {
                                                Header: "Model",
                                                accessor: "model_name"
                                            }, {
                                                Header: "Car Image",
                                                accessor: "thumbnail",
                                                Cell: row => (
                                                    <div className="imgDatatable_react">
                                                        <img src={`${imagePath}${row.value}`}/>
                                                    </div>
                                                )
                                            }, {
                                                Header: "Stock Status",
                                                accessor: "available",
                                                Cell: row => (
                                                    <div className="actionWapper">
                                                        {(row.value=='Y'?"In Stock":"Out Of Stock")}
                                                    </div>
                                                )
                                            }, {
                                                Header: "Manage Stock",
                                                id: "stock",
                                                Cell: row => (
                                                    <div className="actionWapper">
                                                        <button
                                                            type="button" className="btn btn-info btn-fill" data-toggle="tooltip"
                                                            data-placement="top"
                                                            onClick={e => {
                                                                this.getCar_by_id(row.original.car_id,row.original.available);
                                                            }}
                                                        >
                                                             Manage Stock
                                                        </button>  
                                                        
                                                    </div>
                                                )
                                            }
                                        ]
                                    }
                                ]}
                                    defaultPageSize={10}
                                    className="-striped -highlight"/>
                                <br/>
                            </div>
                        </Col>
                    </Row>}>
                    </Card>
                </div>
            </Grid>
            </div>
        )
    }
}

function mapStateToProps({ cars }) {
    //console.log("State", cars.carsStocks);
    return { stocks: cars.carAvailable}
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(carsStocks, dispatch)
    }
}
export default connect(mapStateToProps, { carsStocks, carAvailablesStocks, carUpdateStocks })(CarsMaster)
