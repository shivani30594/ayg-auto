import React, { Component } from 'react';
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import { Field, reduxForm, initialize,FieldArray } from "redux-form";
import Radio from 'elements/CustomRadio/CustomRadio';
import Loader from "../../components/Loader/Loader";
import { Card } from 'components/Card/Card.jsx';
import DropDownSelect from "../../components/Basic/DropDownSelect";
import Button from 'elements/CustomButton/CustomButton.jsx';
import _ from 'lodash';
import Ckeditor from "../../components/CKEditor/Ckeditor";
import TextboxD from "../../components/Dynamic/TextboxD";

const required = value => value
    ? undefined
    : 'This Field Is Required.'
const renderField = ({ id, input, label, type, meta: { touched, error, warning }, disabled, isnotrequired }) => {
    const errorClass = ((touched) && error) ? 'form-group has-error' : 'form-group';
    return (
        <div className={errorClass}>
            <label className="control-label">{label} {isnotrequired === "true" ?'' :<span className="required">*</span>} </label>
            <input id={id} {...input} placeholder={label} type={type} className="form-control" disabled={disabled}/>
            {touched && ((error && <p className="help-block">{error}</p>) || (warning && <p className="help-block">{warning}</p>))}
        </div>
    )
}

const renderTextArea = ({ id, input, label, type, meta: { touched, error, warning }, disabled }) => {
    const errorClass = ((touched) && error) ? 'form-group has-error' : 'form-group';
    return (
        <div className={errorClass}>
            <label className="control-label">{label} </label>
            <textarea id={id} {...input} placeholder={label} type={type} className="form-control" rows="3" cols="40"  >

            </textarea>
            {touched && ((error && <p className="help-block">{error}</p>) || (warning && <p className="help-block">{warning}</p>))}
        </div>


    )
}

const renderFile = ({id,input,label,type}) => {
    let newInput = _.omit(input, ['value'])
return (
    <div>
     <label className="control-label"> {label} </label>   
     <input id={id} {...newInput} type={type}  required className="form-control"/>
     </div>
    )
}

const multipleFile = ({id,input,label,type}) => {
    let newInput = _.omit(input, ['value'])
    return (
        <div className="form-group">
        <label className="control-label"> {label} </label>  
        <input id={id} {...newInput} type={type}  required className="form-control" multiple/>
        </div>    
    )
}

const renderEditor = ({input, rest}) => {
  
   return (
       <div {...input}>
           <Ckeditor input={input}/>
        </div>   
   )
   
}

const categories = ['Economical','Sedan','Coupe','Hatchback','Convertible','Wagon','SUV','Mini-Van','Truck']
const car_type = ['new-cars', 'used-cars','new-and-used-cars']
const car_color = ['White','Black','Grey','Grey-light','Silver','Red','Blue','Orange','Brown']
const car_transmission = ['Automatic','Manual']
const renderMembers = ({ fields,title}) =>{

    return ( 
        <div>
        {fields.map((field, index) => (
                    <Field
                    id={`${field}`}
                    name={`${field}_other`}                                           
                    label={`${title}`}
                    type="text"
                    component={renderField}
                    />
        ))}
        <button type="button" onClick={this.add} className="btn btn-primary mb-20" onClick={() => fields.push({})}>{`Add.${title}`}</button>
        </div>
    )
}

let AddCar = (props) => {
    //console.log('Property',props);
    const { handleSubmit, pristine, reset, submitting } = props    
    return (
        <Grid fluid>         
            <Row>
                <Row> 
                <a href="/admin/#/cars" className="btn btn-primary pull-right">Back To Cars Master </a>
                </Row>
                <Col md={12}>
                    <Card
                        title="Add Cars"
                        content={
                            <form id="addCar" onSubmit={handleSubmit(props.onSubmit)} method="post" encType="multipart/form-data" >
                            <Row>
                                <Col md={4}>
                                <div className="form-group">
                                <label htmlFor="dropDownSelect" className="control-label">Select Body Style</label>
                                <Field
                                name="car_categories"
                                // component="select"
                                label="dropDownSelect"
                                component={DropDownSelect}
                                categories={categories}
                                className="form-control"
                                >
                                </Field>
                                </div>
                                </Col>
                                <Col md={4}>
                                        <Field
                                            id="name"
                                            name="brand"                                           
                                            label="brand"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                   </Col>
                                    <Col md={4}>
                                        <Field
                                            id="name"
                                            name="model_name"                                           
                                            label="Model Name"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                   </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>
                                        <Field
                                            id="name"
                                            name="price"                                           
                                            label="Price"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                   </Col>
                                    <Col md={4}>
                                        <Field
                                            id="name"
                                            name="year"                                           
                                            label="Year"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                   </Col>
                                    <Col md={4}>
                                        <Field
                                            id="name"
                                            name="miles"                                           
                                            label="Miles"
                                            type="text"
                                            component={renderField}
                                            validate={[required]}
                                        />
                                   </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>
                                        <div className="form-group">
                                            <label htmlFor="color_selection" className="control-label">Select Color Of Car</label>
                                                <Field
                                                name="car_color"
                                                label="color_selection"
                                                component={DropDownSelect}
                                                categories={car_color}
                                                className="form-control"
                                                >
                                                </Field>
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                    <div className="form-group">
                                        <label htmlFor="car_transmission" className="control-label">transmission Of Car</label>
                                            <Field
                                                name="car_transmission"
                                                label="color_selection"
                                                component={DropDownSelect}
                                                categories={car_transmission}
                                                className="form-control"
                                                >
                                        </Field>
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                    <Field 
                                        id="thumbnail"
                                        name="file"
                                        type="file"
                                        name="thumbnail"
                                        label="Car Thumbnail"
                                        validate={[required]}
                                        component= {renderFile} />
                                    </Col>
                                </Row>
                                    <h4 className="title ml-15"> Basic Information </h4>
                                    <Row> 
                                    <Col md={3}> 
                                    <Field
                                            id="Passenger_Capacity"
                                            name="passenger_capacity"                                           
                                            label="Passenger Capacity"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                            
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="Wheel"
                                            name="wheel"                                           
                                            label="wheel"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                         
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="tire"
                                            name="tire"                                           
                                            label="Tire"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                           
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="PowerTrain Warranty"
                                            name="powertrain_warranty"                                           
                                            label="PowerTrain Warranty"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                            
                                        />
                                    </Col>
                                    </Row>
                                    <Row> 
                                    <Col md={3}> 
                                    <Field
                                            id="Engine"
                                            name="engine"                                           
                                            label="Engine"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                           
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="Horsepower"
                                            name="horsepower"                                           
                                            label="Horsepower"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                            
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="transmission"
                                            name="transmission"                                           
                                            label="Transmission"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                            
                                        />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="Stock Number"
                                            name="stock_number"                                           
                                            label="Stock Number"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                           
                                        />
                                    </Col>
                                    </Row>
                                    <Row> 
                                    <Col md={3}>                                     
                                    <Field
                                            id="VIN"
                                            name="vin"                                           
                                            label="VIN"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                           
                                    />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="MPG_CITY"
                                            name="mpg_city"                                           
                                            label="MPG CITY"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                            
                                    />
                                    </Col>
                                    <Col md={3}> 
                                    <Field
                                            id="MPG_HWY"
                                            name="mpg_hwy"                                           
                                            label="MPG HWY"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                            
                                    />
                                    </Col>
                                    <Col md={3}>
                                    <Field
                                            id="Drivetrain"
                                            name="drivetrain"                                           
                                            label="Drivetrain"
                                            type="text"
                                            isnotrequired="true"
                                            component={renderField}
                                            
                                    />
                                    </Col> 
                                </Row>
                                <h4 className="title ml-15"> Other Information </h4>
                                <Row>
                                    <Col md={4}>                    
                                    <FieldArray title="ACCESSORY PACKAGES"  name="accessory_packages" component={renderMembers} />                                                                
                                    </Col>
                                    <Col md={4}>                                                                 
                                    <FieldArray title="BRAKING & TRACTION"  name="braking_traction" component={renderMembers} />                                                                 
                                    </Col>
                                    <Col md={4}>                                                                   
                                    <FieldArray title="COMFORT & CONVENIENCE" name="comfort_convenience" component={renderMembers} />                                                                
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>                                                                  
                                    <FieldArray title="ENTERTAINMENT & INSTRUMENTATION" name="entertainment_instrumentation" component={renderMembers} />                                                               
                                    </Col>
                                    <Col md={4}>                                                                 
                                    <FieldArray title="LIGHTING" name="lighting" component={renderMembers} />                                                                
                                    </Col>
                                    <Col md={4}>                                                                  
                                    <FieldArray title="EXTERIOR" name="exterior" component={renderMembers} />                                                               
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>                                                                  
                                    <FieldArray title="SEATS" name="seats" component={renderMembers} />                                                             
                                    </Col>
                                    <Col md={4}>                                                                   
                                    <FieldArray title="SAFETY & SECURITY" name="safety_security" component={renderMembers} />                                                                 
                                    </Col>
                                    <Col md={4}>                                                                 
                                    <FieldArray title="WHEELS & TIRES" name="wheels_tires" component={renderMembers} />                                                                
                                    </Col>
                                </Row>
                                <Row>
                                    <Col md={4}>                                    
                                    <FieldArray title="STEERING" name="steering" component={renderMembers} />                                                                  
                                    </Col>
                                </Row>
                                
                                <h4 className="title ml-15"> Seo For Current Cars </h4>

                                <Row>
                                    <Col md={4}>
                                        <Field
                                            id="meta_title"
                                            name="meta_title"
                                            label="Meta Title"
                                            type="textarea"
                                            component={renderTextArea}
                                            
                                        />
                                    </Col>
                                    <Col md={4}>
                                        <Field
                                            id="meta_description"
                                            name="meta_description"
                                            label="Meta Description"
                                            type="textarea"
                                            component={renderTextArea}
                                            

                                        />
                                    </Col>
                                    <Col md={4}>
                                        <Field
                                            id="keyword"
                                            name="keyword"
                                            label="Keyword"
                                            type="textarea"
                                            component={renderTextArea}
                                            

                                        />

                                    </Col>
                                </Row>
                                
                                <h4 className="title ml-15"> Images And Video </h4>
                                <Row>
                                    <Col md={4}>
                                    <Field 
                                        id="image"
                                        type="file"
                                        name="images[]"
                                        label="Car Images"
                                        className="form-control"                                        
                                        component= {multipleFile} />
                                    </Col>
                                    <Col md={4}>
                                        <div className="form-group">
                                            <label htmlFor="car_type" className="control-label">Select Car Type</label>
                                            <Field
                                                name="car_type"
                                                // component="select"
                                                label="car_type"
                                                component={DropDownSelect}
                                                categories={car_type}
                                                className="form-control"
                                            >
                                            </Field>
                                        </div>
                                    </Col>
                                    <Col md={4}>
                                     <FieldArray title="Youtube" name="video_urls" component={renderMembers} />
                                    </Col>
                                </Row>    
                                <button type="submit" className="btn-fill pull-right btn btn-info">Add</button>
                                <div className="clearfix"></div>
                            </form>
                        }
                    />
                </Col>
            </Row>
        </Grid>
    )
}
AddCar = reduxForm({
    form: 'carForm',
    enableReinitialize: true
})(AddCar)

export default AddCar;
