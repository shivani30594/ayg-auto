import React, {Component} from 'react';
import {render} from "react-dom";
import {Grid, Row, Col} from 'react-bootstrap';
import { Link } from "react-router-dom";
import Loader from "../../components/Loader/Loader";
import {Card} from 'components/Card/Card.jsx';
import getdata from '../../containers/Car/EditCarPage';
import _ from 'lodash';
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import ViewCarMaster from "./ViewCarMaster";
// Import React Table
import {connect} from "react-redux";
import ReactTable from "react-table";
import "../../../node_modules/react-table/react-table.css";
import carApi from "../../api/carApi";
import {deleteCars} from "../../actions/carsAction";
const imagePath = 'http://aygauto.com/backend/public/cars_images/';
class CarsMaster extends Component {
    constructor(props) {

        super();
        this.state = {
            data: [],
            viewdata:[],
            open: false,
            id:''
        };
        
    }
componentDidMount() {
    this.fetchdata(); 
}
fetchdata()
{
    carApi
    .getCars()
    .then(response => {
       
        this.setState({data: response})
    })   
}
 //Alert that you want to delete the data or not*****************************************************
 delete_alert(id) {
    confirmAlert({
      title: "Confirm to submit",
      message: "Are You Sure For Delete This Record .....",
      buttons: [
        {
          label: "Yes",
          onClick: () => this.DeleteData(id)
        },
        {
          label: "No"          
        }
      ]
    });
  }
  
 

  // Delete the data from your database*********************************************************************
  DeleteData(id) {  
      
    this.props.deleteCars(id,()=>{
        this.fetchdata();
    })
  
  }
  

  getCar_by_id(id){

    
    this.setState({ open: true });
    let arr=[];
        let img=[];
        carApi.getCar_by_id(id)
        .then(response=>{           
            try {
                arr  = JSON.parse(response.other_information);           
                img = JSON.parse(response.slider_images);
               
            } catch (error) {
             // console.log("errror") ;
           }
            
           ///Project.splice(index,1);
           response.slider_images =img
           response.accessory_packages = arr.accessory_packages
           response.braking_traction = arr.braking_traction
           response.comfort_convenience = arr.comfort_convenience
           response.entertainment_instrumentation = arr.entertainment_instrumentation
           response.exterior = arr.exterior
           response.lighting = arr.lighting
           response.safety_security = arr.safety_security
           response.seats = arr.seats    
           response.steering = arr.steering    
           response.wheels_tires = arr.wheels_tires    
           response.video_urls = arr.video_urls    
           
           
            this.setState({viewdata:response});
        })
        
        
  };
  
  onCloseModal = () => {
    this.setState({ open: false });
  };
  

   
 
    render() {
      
        return (
            <Grid fluid>
            <ViewCarMaster  viewdata={this.state.viewdata} open={this.state.open}   onCloseModal= {this.onCloseModal} />
                
                <Card
                    title="Car"
                    content={
                    <Row >
                            <Col md={12}>
                                <div className="mb-20 pull-right">
                                    <a href="/admin/#/car/new" className="btn btn-primary">Add Car
                                </a>
                                </div>
                            </Col>
                        <Col md={12}>
                            <div style={{textAlign:"center"}}>
                                <ReactTable
                                    data={this.state.data}
                                    columns={[{
                                        columns: [                                           
                                            {
                                                Header: "Category Name",
                                                accessor: "car_categories"
                                            }, {
                                                Header: "Brand",
                                                accessor: "brand"
                                            }, {
                                                Header: "Model",
                                                accessor: "model_name"
                                            }, {
                                                Header: "Car Image",
                                                accessor: "thumbnail",
                                                Cell: row => (
                                                    <div className="imgDatatable_react">
                                                        <img src={`${imagePath}${row.value}`}/>
                                                    </div>
                                                )
                                            },{
                                                Header: "Action",
                                                id: "action", 
                                                Cell: row => ( 
                                                    <div className="actionWapper">
                                                       <button
                                                            type="button"      className="btn btn-primary btn-fill"       
                                                            onClick={e => {
                                                            this.getCar_by_id(row.original.id);
                                                            }}
                                                        >
                                                        <i className="pe-7s-look"> </i> View 
                                                        </button>   
                                                        <button
                                                        className="btn btn-info btn-fill"
                                                        >
                                                        <Link to={`/car/${row.original.id}`}> <i className="pe-7s-pen"> </i>Edit</Link>
                                                        </button>
                                                        <button
                                                            type="button"  className="btn btn-danger btn-fill"       
                                                            onClick={e => {
                                                            this.delete_alert(row.original.id);
                                                            }}>
                                                           <i className="pe-7s-close"> </i> Delete 
                                                        </button> 
                                                        
                                                        {/*<a style={{cursor: 'pointer'}} name="" onClick={() => this.onDelete(row.value)}>Delete</a>*/}
                                                                                                            
                                                        
                                                    </div>
                                                )
                                            }
                                        ]
                                    }
                                ]}
                                    defaultPageSize={10}
                                    className="-striped -highlight"/>
                                <br/>
                            </div>
                        </Col>
                    </Row>}>
                        </Card>
                
            </Grid>
        )
    }
}


function mapStateToProps({carsCategory}) {
    return {carsCategory}
}
export default connect(mapStateToProps, {deleteCars})(CarsMaster)

