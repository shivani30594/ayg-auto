import React, {Component} from 'react';
import {render} from "react-dom";
import {Grid, Row, Col} from 'react-bootstrap';
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import Loader from "../../components/Loader/Loader";
import {Card} from 'components/Card/Card.jsx';
import _ from 'lodash';
import { Link } from "react-router-dom";
// Import React Table
import ReactTable from "react-table";
import "../../../node_modules/react-table/react-table.css";
import {connect} from "react-redux";
import carApi from "../../api/carApi";
import {deleteCarsCategory} from "../../actions/carCategoryAction";
const imagePath = 'http://aygauto.com/backend/public/ayg_images/';

class CarCategories extends React.Component {
    constructor() {

        super();
        this.state = {
            data: []
        };
    }

    componentDidMount() {
        this.fetchdata();
    }
    fetchdata()
    {
        carApi
        .getCategories()
        .then(response => {
            
            this.setState({data: response})
        })   
    }
    //Alert that you want to delete the data or not*****************************************************
    delete_alert(id) {
        confirmAlert({
        title: "Confirm to submit",
        message: "Are You Sure For Delete This Record .....",
        buttons: [
            {
            label: "Yes",
            onClick: () => this.DeleteData(id)
            },
            {
            label: "No"          
            }
        ]
        });
    }
    
    

    // Delete the data from your database*********************************************************************
    DeleteData(id) { 
        
        this.props.deleteCarsCategory(id,()=>{
            this.fetchdata();
        });
        
      
    }

  
    render() {
        const {data} = this.state;
        return (
            <Grid fluid>
                <Row>
                    <div className="mb-20 pull-right">
                            <a href="/admin/#/car-category/new" className="btn btn-primary">Add Category
                            </a>
                    </div>
                </Row>
                <Row>
                    <Col md={12}>
                        <div style={{'text-align':"center"}}>
                            <ReactTable
                                data={data}
                                columns={[{
                                    columns: [
                                        {
                                            Header: "Category Name",
                                            accessor: "category"
                                        }, {
                                            Header: "Category Image",
                                            accessor: "image",
                                            Cell: row => (
                                                <div className="imgDatatable_react">
                                                    <img src={`${imagePath}${row.value}`}/>
                                                </div>
                                            )
                                        }, {
                                            Header: "Created Date",
                                            accessor: "created_at"
                                        }, {
                                            Header: "Action",
                                            id: "action",
                                            Cell: row => (
                                                <div className="actionWapper">
                                                    <button
                                                     type="button"                                                    
                                                     className="btn btn-info btn-fill" >
                                                        <Link to={`/car-category/${row.original.id}`}>
                                                        <i className="pe-7s-pen"> </i> Edit</Link>
                                                        </button>
                                                    <button
                                                        type="button"                                                           
                                                        className="btn btn-danger btn-fill"                                                         
                                                        onClick={e => {
                                                        this.delete_alert(row.original.id);
                                                        }}>
                                                       <i className="pe-7s-close"> </i> Delete
                                                    </button> 
                                                </div>
                                            )
                                        }
                                    ]
                                }
                            ]}
                                defaultPageSize={10}
                                className="-striped -highlight"/>
                            <br/>
                        </div>
                    </Col>
                </Row>
            </Grid>
        )
    }
}
function mapStateToProps({carsCategory}) {
    return {carsCategory}
}
export default connect(mapStateToProps, {deleteCarsCategory})(CarCategories)   
//export default CarCategories
