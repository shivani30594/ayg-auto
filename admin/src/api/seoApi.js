import * as apiConstant from './apiConstant'
import _ from 'lodash'

class seoApi {
    static getSeo() {
        return fetch(`${apiConstant.API_URL}get-seo?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }


    static getSeo_by_id(id) {
        return fetch(`${apiConstant.API_URL}getseo_by_id/${id}?token=${sessionStorage.getItem('jwt')}`, {
            method: 'get',
        }).then(function (response) {
            return response.json();
        }).catch(function (error) {
            throw error;
        });
    }

    static editSeo(req,id) {
        let form = document.getElementById('editSeoForm');
        let data = new FormData(form);   
        data.append('id',id);
        return fetch(`${apiConstant.API_URL}edit-seo`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                "Authorization": "Bearer " + sessionStorage.getItem('jwt')
            },
            body: data
        })
            .then(function (response) {
                return response.json();
            })
            .catch(function (error) {
                return error;
            });
    }

}

export default seoApi