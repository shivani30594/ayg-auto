import React from "react";
import namor from "namor";
import carApi from "../api/carApi";

const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newPerson = () => {
    carApi.getCategories();
  return {
    catName: namor.generate({ words: 1, numbers: 0 }),
    catImage: namor.generate({ words: 1, numbers: 0 }),
  };
};


export function makeData(len = 25) {
carApi.getCategories();
  return range(len).map(d => {
    return {
      ...newPerson(),
      children: range(10).map(newPerson)
    };
  });
}
