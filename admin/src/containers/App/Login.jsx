import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {Route, Switch, Redirect} from 'react-router-dom';

import NotificationSystem from 'react-notification-system';
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import Header from 'components/Header/Header';
import Footer from 'components/Footer/Footer';
import Sidebar from 'components/Sidebar/Sidebar';
import LoginFrom from 'components/Forms/LoginForm'

import {style} from "variables/Variables.jsx";
import * as sessionActions from "../../actions/sessionAction";
import appRoutes from 'routes/app.jsx';

import '../../assets/css/style.css'
import logo from 'assets/img/logo-white.svg';

class Login extends Component {
    onSubmit = (values) => {
        this.props.actions.loginUser(values)
    }

    render() {
        const session = this.props.session
        if (sessionStorage.getItem('jwt')) {
            this
                .props
                .history
                .push('/#/dashboard')
        }
        const {disptch, login} = this.props 
        return (
            <div>
                <nav className="navbar-primary  navbar-absolute navbar navbar-inverse">
                    <div className="container">
                        <div className="navbar-header"></div>
                    </div>
                </nav>
                <div className="wrapper wrapper-full-page">
                    <div
                        className="full-page login-page"
                        data-color="black"
                        data-image="/static/media/full-screen-image-3.ef9c8d65.jpg">
                        <div className="content">
                            <div className="logo-login align-items-center">
                                <a href="/admin" className="simple-text logo-mini">
                                    <div className="logo-img">
                                        <img src={logo} alt="AYG AUTO" className="img-responsive center-block"/>
                                    </div>

                                </a>
                            </div>
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                                        <LoginFrom onSubmit={this.onSubmit} sessiondata={session}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps({session}) {
    return {session}
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(sessionActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);