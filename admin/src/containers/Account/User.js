import React, {Component} from 'react'
import {connect} from "react-redux"
import {bindActionCreators} from "redux"

import NotificationSystem from 'react-notification-system';
import * as adminActions from "../../actions/adminAction";
import UserProfile from '../../views/UserProfile/UserProfile'
import adminApi from "api/adminApi";


class User extends Component {
    componentDidMount() {
        this.props.actions.adminRequest();
    }
    onSubmit = (values) => {       
        adminApi.updateUser(values).then(()=>{
            document.getElementById("password").value = "";
            document.getElementById("password_repeat").value = "";
        });

    }
    render() {
        const {admin} = this.props
        return (
            <div className="content">
                <UserProfile
                    userdata={admin}
                    initialValues={admin.user}
                    onSubmit={this.onSubmit}/>
            </div>
        )
    }
}

function mapStateToProps({admin}) {
    return {admin}
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(adminActions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(User)