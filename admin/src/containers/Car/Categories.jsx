import React, {Component} from 'react'
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';
import {bindActionCreators} from "redux"

import NotificationSystem from 'react-notification-system';
import * as adminActions from "../../actions/adminAction";
import CarCategories from '../../views/CarsMaster/CarCategories';
import { Card } from 'components/Card/Card.jsx';
import carApi from "api/carApi";

import "../../../node_modules/react-table/react-table.css";

class Categories extends Component {

    onSubmit = (values) => {
      
        carApi.addCategory(values);
    }
    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <Card title="Categories" content={
                                <div>
                                <CarCategories  />
                                </div>
                            }>
                            </Card>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}


  export default Categories;