import React, {Component} from 'react'
import {
    Grid, Row, Col,
    FormGroup, ControlLabel, FormControl
} from 'react-bootstrap';

import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import NotificationSystem from 'react-notification-system';
import * as adminActions from "../../actions/adminAction";
import CarCategories from '../../views/CarsMaster/AddCategory';
import { Card } from 'components/Card/Card.jsx';
import carApi from "api/carApi";
import {postCarsCategory} from "../../actions/carCategoryAction";
class AddCategories extends Component {

    onSubmit = (values) => {

        this.props.postCarsCategory(values,()=>{
            this.props.history.push('/car-categories');
        });
       
    }
    render() {
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                        <CarCategories onSubmit={this.onSubmit}/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

function mapStateToProps({carsCategory}) {
    return {carsCategory}
}
export default connect(mapStateToProps, {postCarsCategory})(AddCategories) 

