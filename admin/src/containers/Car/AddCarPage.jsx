import React, {Component} from 'react'
import {
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
} from 'react-bootstrap';
import {connect} from "react-redux"
import {bindActionCreators} from "redux";
import {Card} from 'components/Card/Card.jsx';
import carApi from "api/carApi";
import AddCar from "../../views/CarsMaster/AddCar";
import {postCars} from "../../actions/carsAction";

class AddCarPage extends Component {
    onSubmit = (values) => {
        this.props.postCars(values,()=>{
            this.props.history.push('/cars');
        });
    }
    render() {
        if(this.props.cars.addCar === true){
           
        }
        else{
            
        }
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <AddCar onSubmit={this.onSubmit}/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}
function mapStateToProps({cars}) {
    return {cars}
}
export default connect(mapStateToProps, {postCars})(AddCarPage)