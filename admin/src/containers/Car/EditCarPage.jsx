import React, {Component} from 'react'
import {
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
} from 'react-bootstrap';
import _ from "lodash";
import {connect} from "react-redux"
import {bindActionCreators} from "redux";
import {Card} from 'components/Card/Card.jsx';
import carApi from "api/carApi";
import EditCar from "../../views/CarsMaster/EditCar";
import {deleteImg,updateCars} from "../../actions/carsAction";

class EditCarPage extends Component {
    constructor(props)
    {
        super()
        this.state={
            data:[]
        }
    }
    componentDidMount() {
       this.fetchdata(this.props.match.params.id);
    }


    fetchdata(id)
    {
        let arr=[];
        let img=[];
        carApi.getCar_by_id(id)
        .then(response=>{    
                
            try {
                arr  = JSON.parse(response.other_information);           
                img = JSON.parse(response.slider_images);
            } catch (error) {
             
           }
            
           ///Project.splice(index,1);
           
           response.slider_images =img
           response.accessory_packages = arr.accessory_packages
           response.braking_traction = arr.braking_traction
           response.comfort_convenience = arr.comfort_convenience
           response.entertainment_instrumentation = arr.entertainment_instrumentation
           response.exterior = arr.exterior
           response.lighting = arr.lighting
           response.safety_security = arr.safety_security
           response.seats = arr.seats    
           response.steering = arr.steering    
           response.wheels_tires = arr.wheels_tires    
           response.video_urls = arr.video_urls    
           
           
            this.setState({data:response});
        })
        
    }
    
    deleteImage=(id,item)=>
    {
        this.props.deleteImg(this.props.match.params.id,id,()=>{
            
            this.fetchdata(this.props.match.params.id); 
        });
       window.location.reload();  
       
    }
    onSubmit = (values) => { 
              
        this.props.updateCars(values,()=>{
            this.props.history.push('/cars');
        })
        
    }

    render() {  
        
        
       
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col md={12}>
                            <EditCar deleteImage={this.deleteImage} initialValues={this.state.data} onSubmit={this.onSubmit}/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}
function mapStateToProps({cars}) {
    return {cars}
}
export default connect(mapStateToProps, {deleteImg,updateCars})(EditCarPage)