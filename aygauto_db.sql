-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 14, 2019 at 09:31 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aygauto_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `car_categories` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `miles` int(11) NOT NULL,
  `car_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `car_transmission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `car_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`id`, `car_categories`, `brand`, `model_name`, `price`, `year`, `miles`, `car_color`, `car_transmission`, `thumbnail`, `car_type`, `created_at`, `updated_at`) VALUES
(80, 'Mini-Van', 'Dodge', 'Grand Caravan', 7995, 2010, 122120, 'Black', 'Automatic', '1525241761.JPG', 'new-and-used-cars', '2018-05-02 06:16:01', '2018-06-05 03:41:30'),
(2, 'Truck', 'Ford', 'F150', 9999, 2009, 264300, 'White', 'Automatic', '1524719193.jpg', 'new-and-used-cars', '2018-04-26 05:06:33', '2018-05-15 20:50:30'),
(11, 'Sedan', 'Nissan', 'Altima', 11999, 2017, 36590, 'Grey-light', 'Automatic', '1524807008.jpg', NULL, '2018-04-27 05:30:08', '2018-04-27 05:30:08'),
(79, 'Sedan', 'Volkswagen', 'Jetta', 5490, 2010, 79402, 'Black', 'Automatic', '1525241574.jpg', 'new-and-used-cars', '2018-05-02 06:12:54', '2018-05-26 19:34:08'),
(75, 'Coupe', 'Kia', 'Forte', 4999, 2010, 135693, 'White', 'Automatic', '1525238706.jpg', 'used-cars', '2018-05-02 05:16:32', '2018-05-15 20:43:23'),
(81, 'Sedan', 'Kia', 'Optima', 8999, 2014, 76731, 'White', 'Automatic', '1525241898.JPG', 'new-and-used-cars', '2018-05-02 06:18:18', '2018-05-26 19:39:48'),
(82, 'SUV', 'Honda', 'CR-V EX-L', 16250, 2014, 55676, 'Grey', 'Automatic', '1525242492.jpg', 'new-and-used-cars', '2018-05-02 06:28:12', '2018-05-19 19:52:11'),
(83, 'Coupe', 'Chevy', 'Cruz LTZ', 8999, 2012, 86295, 'Orange', 'Automatic', '1525242805.jpg', 'new-and-used-cars', '2018-05-02 06:33:25', '2018-05-15 20:50:07'),
(84, 'Mini-Van', 'Dodge', 'Grand Caravan', 8499, 2014, 129450, 'Blue', 'Automatic', '1525243111.jpg', 'used-cars', '2018-05-02 06:38:31', '2018-05-15 20:49:06'),
(85, 'Sedan', 'Toyota', 'Camry', 8299, 2012, 116864, 'Silver', 'Automatic', '1525243357.jpg', 'new-and-used-cars', '2018-05-02 06:42:37', '2018-06-05 04:29:18'),
(86, 'Sedan', 'Mercedes', 'Benz C300', 9399, 2010, 118504, 'White', 'Automatic', '1525243659.jpg', 'new-and-used-cars', '2018-05-02 06:47:39', '2018-05-26 20:03:04'),
(88, 'Sedan', 'Hyundai', 'Elantra GLS', 6799, 2013, 93956, 'Black', 'Automatic', '1525244909.jpg', NULL, '2018-05-02 07:08:29', '2018-05-02 07:08:29'),
(87, 'SUV', 'Nissan', 'Murano', 9499, 2009, 63787, 'Grey', 'Automatic', '1525244119.jpg', NULL, '2018-05-02 06:55:19', '2018-05-02 06:55:19'),
(78, 'Economical', 'Kia', 'Rio', 5999, 2013, 96428, 'Black', 'Automatic', '1525241372.jpg', 'new-and-used-cars', '2018-05-02 06:09:32', '2018-05-26 19:56:25'),
(72, 'Sedan', 'Honda', 'Accord', 7999, 2009, 81606, 'Silver', 'Automatic', '1525236390.JPG', 'new-and-used-cars', '2018-05-02 04:46:30', '2018-05-26 19:57:17'),
(94, 'Sedan', 'Toyota', 'Camry', 8999, 2012, 88103, 'White', 'Automatic', '1525451418.jpg', 'used-cars', '2018-05-04 16:30:18', '2018-05-04 16:30:18'),
(95, 'SUV', 'Nissan', 'Pathfinder', 6999, 2006, 121059, 'Red', 'Automatic', '1525636106.JPG', 'used-cars', '2018-05-06 19:48:26', '2018-05-06 19:48:26'),
(96, 'Sedan', 'NIssan', 'Sentra', 6899, 2013, 117646, 'Brown', 'Automatic', '1525636374.jpg', 'used-cars', '2018-05-06 19:52:54', '2018-05-06 19:52:54'),
(97, 'Sedan', 'Mercedes', 'c300', 23999, 2015, 29900, 'White', 'Automatic', '1527371542.jpg', 'new-and-used-cars', '2018-05-26 21:52:22', '2018-05-26 21:52:22'),
(98, 'Sedan', 'Mercedes', 'C250', 13999, 2013, 63429, 'White', 'Automatic', '1528750175.jpg', 'new-and-used-cars', '2018-06-07 00:57:37', '2018-06-11 20:49:35');

-- --------------------------------------------------------

--
-- Table structure for table `cars_seo`
--

DROP TABLE IF EXISTS `cars_seo`;
CREATE TABLE IF NOT EXISTS `cars_seo` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `car_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cars_seo`
--

INSERT INTO `cars_seo` (`id`, `car_id`, `meta_title`, `meta_description`, `keyword`) VALUES
(1, '80', 'Are you looking for used cars for sale in Florida? - Aygauto', 'Now buying the used cars in Florida is as easy & economical as you could wish. We have got a huge variety of top-class brands including Volkswagens, Honda, Nissan, Hyundai , Toyota, and many more.', 'Buying the used car in Florida, used cars for sale in Florida');

-- --------------------------------------------------------

--
-- Table structure for table `cars_stock`
--

DROP TABLE IF EXISTS `cars_stock`;
CREATE TABLE IF NOT EXISTS `cars_stock` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `available` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cars_stock`
--

INSERT INTO `cars_stock` (`id`, `car_id`, `available`, `created_at`, `updated_at`) VALUES
(86, 80, 'Y', NULL, NULL),
(81, 75, 'Y', NULL, NULL),
(90, 84, 'Y', NULL, NULL),
(89, 83, 'Y', NULL, NULL),
(78, 72, 'Y', NULL, NULL),
(88, 82, 'N', NULL, NULL),
(16, 2, 'Y', '2018-04-27 05:54:16', NULL),
(104, 98, 'Y', NULL, NULL),
(103, 97, 'Y', NULL, NULL),
(102, 96, 'Y', NULL, NULL),
(101, 95, 'N', NULL, NULL),
(100, 94, 'Y', NULL, NULL),
(94, 88, 'Y', NULL, NULL),
(93, 87, 'Y', NULL, NULL),
(92, 86, 'N', NULL, NULL),
(91, 85, 'Y', NULL, NULL),
(87, 81, 'N', NULL, NULL),
(85, 79, 'Y', NULL, NULL),
(84, 78, 'Y', NULL, NULL),
(74, 11, 'Y', '2018-04-27 13:25:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `car_categories`
--

DROP TABLE IF EXISTS `car_categories`;
CREATE TABLE IF NOT EXISTS `car_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `car_categories`
--

INSERT INTO `car_categories` (`id`, `category`, `image`, `created_at`, `updated_at`) VALUES
(9, 'Economical', '1522318162.png', '2018-03-29 04:39:22', '2018-03-29 04:39:22'),
(8, 'truck', '1525667552.png', '2018-03-29 04:39:01', '2018-05-07 04:32:32'),
(7, 'SUV', '1525667612.png', '2018-03-29 04:38:45', '2018-05-07 04:33:32'),
(10, ' Sedan', '1525667739.png', '2018-03-29 04:39:42', '2018-05-07 04:35:39'),
(11, 'Coupe', '1525668220.png', '2018-03-29 04:39:55', '2018-05-07 04:43:40'),
(12, 'Hatchback', '1525667759.png', '2018-03-29 04:40:05', '2018-05-07 04:35:59'),
(13, 'Convertible', '1525667659.png', '2018-03-29 04:40:23', '2018-05-07 04:34:19'),
(15, 'Wagon', '1525668400.png', '2018-03-29 04:41:22', '2018-05-07 04:46:40'),
(16, 'Mini-Van', '1525668326.png', '2018-03-29 04:41:42', '2018-05-08 12:58:52');

-- --------------------------------------------------------

--
-- Table structure for table `car_details`
--

DROP TABLE IF EXISTS `car_details`;
CREATE TABLE IF NOT EXISTS `car_details` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `car_id` int(11) NOT NULL,
  `passenger_capacity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wheel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tire` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `powertrain_warranty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `engine` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `horsepower` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transmission` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stock_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mpg_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mpg_hwy` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `drivetrain` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `other_information` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `slider_images` text COLLATE utf8_unicode_ci NOT NULL,
  `video_urls` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=99 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `car_details`
--

INSERT INTO `car_details` (`id`, `car_id`, `passenger_capacity`, `wheel`, `tire`, `powertrain_warranty`, `engine`, `horsepower`, `transmission`, `stock_number`, `vin`, `mpg_city`, `mpg_hwy`, `drivetrain`, `other_information`, `slider_images`, `video_urls`, `created_at`, `updated_at`) VALUES
(80, 80, '8', '4', 'tesla', 'yes', 'V8', 'yes', 'Automatic', '008', '2D4RN5DX0AR447953', '17', '25', '4WD', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"174246IMG_0046.JPG\",\"174209IMG_0034.JPG\",\"174139IMG_0017.JPG\",\"173941IMG_0009-1.JPG\",\"061601IMG_0003.JPG\",\"061601IMG_0004.JPG\"]', '', '2018-05-02 06:16:01', '2018-05-26 17:42:46'),
(81, 81, '', '', '', '', '', '', '', '', 'KNAGM4A77E5466022', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"061818IMG_0007.JPG\"]', '', '2018-05-02 06:18:18', '2018-05-26 19:39:40'),
(72, 72, '', '', '', '', '', '', '', '', '', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"195831IMG_0086.JPG\",\"195807IMG_0091.JPG\",\"195752IMG_0094.JPG\",\"195733IMG_0063.JPG\",\"195717IMG_0060.JPG\",\"044630IMG_0059.JPG\"]', '', '2018-05-02 04:46:30', '2018-05-26 19:58:31'),
(2, 2, '4', '4', 'alloy', 'Yes', 'V8', '300', 'Automatic', '008', '1FTPW14V59FA54607', '14', '20', '4WD', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"200056IMG_0066-1.JPG\",\"200041IMG_0062-1.JPG\",\"200024IMG_0054.JPG\",\"175340IMG_0053.JPG\",\"175326IMG_0051-1.JPG\",\"050633Webp.net-compress-image.jpg\"]', '', '2018-04-26 05:06:33', '2018-05-26 20:00:56'),
(11, 11, '4', 'Alloy', 'Yes', 'FWD', '4 Cylinder 2.5Liter', '270', 'Automatic Xtronic CVT', '001', '1N4AL3AP5HN317592', '27', '39', 'FWD', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"0530082018-04-23-PHOTO-00000358.jpg\",\"0530082018-04-23-PHOTO-00000359.jpg\",\"0530082018-04-23-PHOTO-00000360.jpg\",\"0530082018-04-23-PHOTO-00000361.jpg\",\"0530082018-04-23-PHOTO-00000362.jpg\",\"0530082018-04-23-PHOTO-00000363.jpg\",\"0530082018-04-23-PHOTO-00000364.jpg\",\"0530082018-04-23-PHOTO-00000365.jpg\",\"0530082018-04-23-PHOTO-00000366.jpg\",\"0530082018-04-23-PHOTO-00000367.jpg\",\"0530082018-04-23-PHOTO-00000368.jpg\",\"0530082018-04-23-PHOTO-00000369.jpg\",\"0530082018-04-23-PHOTO-00000370.jpg\",\"0530082018-04-23-PHOTO-00000371.jpg\",\"0530082018-04-23-PHOTO-00000372.jpg\",\"0530082018-04-23-PHOTO-00000373.jpg\",\"0530082018-04-23-PHOTO-00000374.jpg\",\"0530082018-04-23-PHOTO-00000375.jpg\"]', '', '2018-04-27 05:30:08', '2018-05-26 19:33:29'),
(7, 7, 'Coupe', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '5', '{\"accessory_packages\":[\"adsasd\",\"asdasd\"],\"braking_traction\":[\"sdad\",\"asdasd\"],\"comfort_convenience\":[\"adsads\",\"asdasd\"],\"entertainment_instrumentation\":[\"adssad\",\"asdsad\"],\"exterior\":[\"asdasd\",\"asdsad\"],\"lighting\":[\"asdasd\",\"adsads\"],\"safety_security\":[\"dsadsad\",\"asddasa\"],\"seats\":[\"asdads\",\"sadasd\"],\"steering\":[\"asdads\",\"addad\"],\"video_urls\":null,\"wheels_tires\":[\"adsad\",\"dsadsadsa\"]}', '[\"051329BMW-5-Series-Right-Front-Three-Quarter-101056.gif\"]', '', '2018-04-26 05:13:29', '2018-04-27 10:18:02'),
(98, 98, '5', '', '', '', '', '', '', '', 'WDDGF4HB0DR250288', '20', '27', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"\"]', '', '2018-06-07 00:57:37', '2018-06-11 20:49:35'),
(97, 97, '', '', '', '', '', '', '', '', '55SWF4JB5FU053074', '25', '34', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"204034carfax.jpg\",\"204034moonroof.jpg\",\"204034roof.jpg\",\"204034seat sound.jpg\",\"204034tire.jpg\",\"204034wheel.jpg\",\"215222c300-2.jpg\",\"215222c300.jpg\"]', '', '2018-05-26 21:52:22', '2018-06-09 20:40:34'),
(96, 96, '', '', '', '', '', '', '', '', '3N1AB7AP8DL761048', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"1952542018-05-05-PHOTO-00000454.jpg\",\"1952542018-05-05-PHOTO-00000455.jpg\",\"1952542018-05-05-PHOTO-00000456.jpg\",\"1952542018-05-05-PHOTO-00000457.jpg\",\"1952542018-05-05-PHOTO-00000458.jpg\",\"1952542018-05-05-PHOTO-00000459.jpg\",\"1952542018-05-05-PHOTO-00000460.jpg\",\"1952542018-05-05-PHOTO-00000461.jpg\",\"1952542018-05-05-PHOTO-00000462.jpg\",\"1952542018-05-05-PHOTO-00000463.jpg\",\"1952542018-05-05-PHOTO-00000464.jpg\",\"1952542018-05-05-PHOTO-00000465.jpg\",\"1952542018-05-05-PHOTO-00000466.jpg\",\"1952542018-05-05-PHOTO-00000467.jpg\",\"1952542018-05-05-PHOTO-00000468.jpg\",\"1952542018-05-05-PHOTO-00000469.jpg\",\"1952542018-05-05-PHOTO-00000470.jpg\",\"1952542018-05-05-PHOTO-00000471.jpg\",\"1952542018-05-05-PHOTO-00000472.jpg\"]', '', '2018-05-06 19:52:54', '2018-05-06 19:52:54'),
(88, 88, '', '', '', '', '', '', '', '', 'KMHDH4AE1DU715066', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"0708291.jpg\",\"0708292.jpg\",\"0708293.jpg\",\"0708294.jpg\",\"0708295.jpg\",\"0708296.jpg\",\"0708297.jpg\",\"0708298.jpg\"]', '', '2018-05-02 07:08:29', '2018-05-03 06:57:43'),
(87, 87, '', '', '', '', '', '', '', '', 'JN8AZ18U59W104666', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"0655191.jpg\",\"0655192.jpg\",\"0655193.jpg\",\"0655194.jpg\",\"0655195.jpg\",\"0655196.jpg\",\"0655197.jpg\",\"0655198.jpg\",\"0655199.jpg\",\"06551910.jpg\"]', '', '2018-05-02 06:55:19', '2018-05-03 06:57:57'),
(86, 86, '', '', '', '', '', '', '', '', 'WDDGF8BB0AF449791', '18', '26', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":[\"Premium Sound\"],\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"0647391.jpg\",\"0647393.jpg\"]', '', '2018-05-02 06:47:39', '2018-06-07 01:02:22'),
(85, 85, '5', '', '', '', '', '', '', '', '4T1BF1FK8CU064964', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"0642371.jpg\",\"0642372.jpg\",\"0642373.jpg\",\"0642374.jpg\",\"0642375.jpg\",\"0642376.jpg\",\"0642377.jpg\"]', '', '2018-05-02 06:42:37', '2018-06-05 04:29:18'),
(84, 84, '', '', '', '', '', '', '', '', '2C4RDGBG6ER379596', '17', '25', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"0638311.jpg\",\"0638312.jpg\",\"0638313.jpg\",\"0638314.jpg\",\"0638315.jpg\",\"0638316.jpg\"]', '', '2018-05-02 06:38:31', '2018-05-15 20:49:06'),
(83, 83, 'Coupe', '', '', '', '', '', '', '', '1G1PH5SC4C7316517', '26', '38', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"0633251.jpg\",\"0633252.jpg\",\"0633253.jpg\",\"0633254.jpg\",\"0633255.jpg\",\"0633256.jpg\",\"0633257.jpg\",\"0633258.jpg\",\"0633259.jpg\"]', '', '2018-05-02 06:33:25', '2018-05-15 20:50:07'),
(82, 82, '', '', '', '', '', '', '', '', '2HKRM3H74EH504754', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"0628121.jpg\"]', '', '2018-05-02 06:28:12', '2018-05-26 19:41:27'),
(95, 95, '', '', '', '', '', '', '', '', '5N1AR18W16C610811', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"194826IMG_0095.JPG\"]', '', '2018-05-06 19:48:26', '2018-05-06 19:48:26'),
(94, 94, '', '', '', '', '', '', '', '', '4T1BF1FK9CU578886', '25', '35', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"1630182018-05-03-PHOTO-00000427.jpg\",\"1630182018-05-03-PHOTO-00000428.jpg\",\"1630182018-05-03-PHOTO-00000429.jpg\",\"1630182018-05-03-PHOTO-00000430.jpg\",\"1630182018-05-03-PHOTO-00000431.jpg\",\"1630182018-05-03-PHOTO-00000432.jpg\",\"1630182018-05-03-PHOTO-00000433.jpg\",\"1630182018-05-03-PHOTO-00000434.jpg\",\"1630182018-05-03-PHOTO-00000435.jpg\",\"1630182018-05-03-PHOTO-00000436.jpg\",\"1630182018-05-03-PHOTO-00000437.jpg\",\"1630182018-05-03-PHOTO-00000438.jpg\",\"1630182018-05-03-PHOTO-00000439.jpg\",\"1630182018-05-03-PHOTO-00000440.jpg\",\"1630182018-05-03-PHOTO-00000441.jpg\",\"1630182018-05-03-PHOTO-00000442.jpg\",\"1630182018-05-03-PHOTO-00000443.jpg\",\"1630182018-05-03-PHOTO-00000444.jpg\",\"1630182018-05-03-PHOTO-00000445.jpg\"]', '', '2018-05-04 16:30:18', '2018-05-26 20:04:33'),
(75, 75, '', '', '', '', '', '', '', '', 'KJHDSFKJ546785467', '25', '34', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"1938272018-04-25-PHOTO-00000396.jpg\",\"1938082018-04-25-PHOTO-00000400.jpg\",\"1937502018-04-25-PHOTO-00000393.jpg\",\"1937062018-04-25-PHOTO-00000391.jpg\",\"1936502018-04-25-PHOTO-00000388.jpg\",\"1936282018-04-25-PHOTO-00000382.jpg\",\"0525062018-04-25-PHOTO-00000379.jpg\",\"0525062018-04-25-PHOTO-00000380.jpg\",\"0525062018-04-25-PHOTO-00000381.jpg\"]', '', '2018-05-02 05:16:32', '2018-05-26 19:38:27'),
(78, 78, '', '', '', '', '', '', '', '', 'KNADM4A35D6209868', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"1956472018-04-23-PHOTO-00000269.jpg\",\"1956252018-04-23-PHOTO-00000268.jpg\",\"0609322018-04-23-PHOTO-00000258.jpg\",\"0609322018-04-23-PHOTO-00000259.jpg\",\"0609322018-04-23-PHOTO-00000260.jpg\",\"0609322018-04-23-PHOTO-00000261.jpg\"]', '', '2018-05-02 06:09:32', '2018-05-26 19:56:47'),
(79, 79, '', '', '', '', '', '', '', '', '3VWJX7AJ5AM002564', '', '', '', '{\"accessory_packages\":null,\"braking_traction\":null,\"comfort_convenience\":null,\"entertainment_instrumentation\":null,\"exterior\":null,\"lighting\":null,\"safety_security\":null,\"seats\":null,\"steering\":null,\"video_urls\":null,\"wheels_tires\":null}', '[\"1935432018-04-23-PHOTO-00000349.jpg\",\"1934462018-04-23-PHOTO-00000354.jpg\",\"1934322018-04-23-PHOTO-00000345.jpg\",\"1934082018-04-23-PHOTO-00000340.jpg\",\"0612542018-04-23-PHOTO-00000337.jpg\",\"0612542018-04-23-PHOTO-00000338.jpg\",\"0612542018-04-23-PHOTO-00000339.jpg\"]', '', '2018-05-02 06:12:54', '2018-05-26 19:35:43');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

DROP TABLE IF EXISTS `seo`;
CREATE TABLE IF NOT EXISTS `seo` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keyword` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `route`, `meta_title`, `meta_description`, `keyword`) VALUES
(1, 'http://www.aygauto.com/', 'Looking for Used Cars for Sale in Florida - Aygauto', 'Looking for used car in perfect condition and right price? Call AYG for the best deals.', 'used cars Florida, used cars for sale in Florida, used car Florida sale'),
(2, 'http://www.aygauto.com/cars', ' Wondering how to get used cars in Florida? - Aygauto', ' Search our inventory of certified used cars for sale in Florida at Enterprise Car Sales. You will get a large variety of quality cars in different brands, models, body shape & color with price just according to your budget scales.	', 'Buying the used car in Florida, used cars for sale in Florida');

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

DROP TABLE IF EXISTS `testimonial`;
CREATE TABLE IF NOT EXISTS `testimonial` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL,
  `description` varchar(250) NOT NULL,
  `image` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `title`, `description`, `image`) VALUES
(1, 'Jeff\'s New Kia Optima', 'AYG Auto found me the best price in tampa bay for my son\'s graduation gift.', 'feedback.jpg'),
(2, 'Bella\'s New Nissan Pathfinder', 'AYG Auto had excellent customer service and got me the best price in all of central florida.', '1525778461.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Hareen', 'had.narola@gmail.com', '$2y$10$A9WMOvHDbeTelS2psJVyKuUdMJgXrXrWwc4CkDm7x/bvNe9G./HRm', NULL, '2018-03-16 01:40:17', '2018-03-16 01:40:17'),
(2, 'Dev', 'dev.narola@gmail.com', '$2y$10$SlnH0aENqLQmqTC3E0DW.OitOecNbUD8wSHSuwcyRsJATSdMxdk0.', NULL, '2018-03-16 01:50:06', '2018-03-16 01:50:06'),
(3, 'mga', 'mga.narola@gmail.com', '$2y$10$ENBsMa.6zHaAgTiV3R2W4O/iPm2.HpUjm7DGvCVlciJMx6TmcLoPO', NULL, '2018-03-16 06:13:44', '2018-03-16 06:13:44'),
(4, 'admin', 'admin@aygauto.com', '$2y$10$ZVgNK5lT5aqm06ri.PlizODPB8BVnr9TbXeJAKA2SlVm5jyrelAaq', NULL, '2018-03-17 06:40:12', '2018-03-21 12:55:29');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
